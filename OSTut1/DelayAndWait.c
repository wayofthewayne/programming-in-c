#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "utils.h"

int main(int argc, const char **argv) {

    // Expecting only one argument
    if (argc > 2) {
        fprintf(stderr, "Only one argument should be passed\n");
        return EXIT_FAILURE;
    }

    // Get number from argv and validate it
    int t;
    if (!get_number(argv[1], &t) || t < 0) {
        fprintf(stderr, "Provided argument is not a number, or it is out of range\n");
        return EXIT_FAILURE;
    }

    // Fork
    pid_t pid_result = fork();
    if (pid_result == -1) {
        perror("fork failed");
        return EXIT_FAILURE;
    }

    if (pid_result == 0) {
        // Child
        sleep((unsigned int)t);
        printf("child exiting\n");
    } else {
        // Parent
        if (wait(NULL) == -1) {
            perror("Error on wait");
            return EXIT_FAILURE;
        }

        printf("child terminated, parent exiting\n");
    }

    return EXIT_SUCCESS;
}
