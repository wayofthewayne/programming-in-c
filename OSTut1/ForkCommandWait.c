#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, const char **argv) {


    pid_t pid_result = fork();

    //Checking for forking errors
    if (pid_result == -1) {
        perror("fork failed");
        return EXIT_FAILURE;
    }


    if (pid_result > 0) {

        //Checking if wait() was successful in parent
        if (wait(NULL) == -1) {
            perror("wait failed");
            return EXIT_FAILURE;
        }

//The child would have terminated and the parent will exit
        printf("Parent exiting after waiting\n");
        return EXIT_SUCCESS;
    }


    // This is the child process (i.e pid_result == 0)
    char *const args[] = {"/bin/ps", "-f", NULL};
    if (execvp(args[0], args) == -1) {
        perror("execvp failed");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
