#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, const char **argv) {


    pid_t pid_result = fork();

    //Checking for forking errors
    if (pid_result == -1) {
        perror("fork failed");
        return EXIT_FAILURE;
    }

    //Checking for parent process
    if (pid_result > 0) {
        printf("Parent Exiting\n");
        return EXIT_SUCCESS;
    }

    // This is the child process (i.e pid_result == 0)
    char *const args[] = {"/bin/ps", "-f", NULL};
    if (execvp(args[0], args) == -1) {
        perror("execvp failed");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
