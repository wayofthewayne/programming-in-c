#include <stdio.h>

int main (int argc, char *argv[]){
    //Note argc stores the amount of arguments, (including the name)
    //argv stores the arguments themselves

    for (int i = 0; i < argc; ++i) //Will print command line arguments
        printf("argv[%d] = %s\n", i, argv[i]);
    return 0;
}