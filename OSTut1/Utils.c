#include <stdlib.h>
#include <limits.h>
#include <errno.h>

#include "utils.h"

/**
 * get_number validates the input string and on success
 * returns a number. Success is determined by the return value.
 * @param input The input string
 * @param number A pointer to an integer where the output is placed
 * @return true on success, false otherwise
 **/
bool get_number(const char *input, int *number) {
    // Check whether string is invalid or empty
    if (input == NULL || *input == '\0')
        return false;

    // Try to parse string and get numeric value
    errno = 0;
    char *ptToNext;
    long num = strtol(input, &ptToNext, 10);

    // if ptToNext does not point to null (after the number) OR
    // if errno was set, some error occured
    if (*ptToNext != '\0' ||  errno != 0)
        return false;

    // Check if this long fits in an int
    if (num < INT_MIN || num > INT_MAX)
        return false;

    *number = (int)num;
    return true;
}