#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char **argv) {
    pid_t pid_result = fork();
    if (pid_result == -1) {
        perror("fork failed");
        return EXIT_FAILURE;
    }

    if (pid_result > 0) {
        if (wait(NULL) == -1) {
            perror("wait failed");
            return EXIT_FAILURE;
        }

        printf("Parent exiting after waiting\n");
        return EXIT_SUCCESS;
    }

    // This is the child process
    ++argv; //We increment to skip filename; Notice the dereference.
    if (execvp(*argv, argv) == -1) {
        perror("execvp failed");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
