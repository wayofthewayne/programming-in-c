#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(void) {
    printf("Executing top -d 2 -n 10 using execl\n");

    const char *bin_name = "top";


    //bin_name has to be entered twice for -d and 2
    //i.e one for -d and one for 2
    //same logic applies to -n and 1o
    if (execlp(bin_name, bin_name, "-d", "2", "-n", "10", NULL) == -1) {
        perror("execlp failed");
        return EXIT_FAILURE;
    }

    // This return should never be reached
    printf("This never showing message");
    return EXIT_SUCCESS;
}