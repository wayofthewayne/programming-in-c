#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "linenoise.h"

int main(void) {

    // Get input and echo here
    char *line;
    const char *delimiter = " ";

    while((line = linenoise("myshell> ")) != NULL) {
        // Tokenise
        const char *token = strtok(line, delimiter); //Getting first token

        while(token != NULL) { //walk through other tokens
            printf("%s\n", token);
            token = strtok(NULL, delimiter);
            //To continue from last token
        }

        // Free the string allocated by linenoise
        linenoiseFree(line);
    }

    printf("Exiting\n");
    return EXIT_SUCCESS;
}