#include <stdio.h>
#include <stdbool.h>
void print_string(char *p_string, int p_count, bool p_reverse);
void helloLoop(void);

//#define HELLO_WORLD

//This Program is an example of conditional compilation
//To compile other program remove comment for define
int main (void){
#ifdef HELLO_WORLD
    helloLoop();
#else
    char *string = "Attach me!\0";
    print_string(string, 0, true);
#endif
}

void helloLoop(void){
    for(int i=0; i<10; ++i)
        printf("Hello, World!\n");
}

void print_string(char *p_string, int p_count, bool p_reverse){
    for(int i=10; i>p_count; --i){
        printf("%d\t%s\n", i, p_string);

        if(i<p_count){
            p_reverse = true;
        }else{
            p_reverse = false;
        }
    }

    if(p_reverse == true){
        printf("Ascending loop");
    }else{
        printf("Descending loop");
    }
}

