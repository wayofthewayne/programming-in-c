#include <stdio.h>

//Environment variables are a set of dynamic named values that can affect the way running processes will behave on a computer.
int main (int argc,char *argv[], char *envp[]){

    for(int i=0; envp[i] != NULL; i++){
        printf("envp[%d] = %s", i , envp[i]);
        //Note how they have a number and value associated with them. 
    }
    return 0;
}