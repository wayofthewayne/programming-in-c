#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

int main (){
    printf("We will attempt to run a non existent program and handle the error");
    char * args[] = {"/usr/bin/tp", "-d", "2", "-n", "10", NULL};

    if(execvp(args[0], args) == -1){
        char errorBuffer[100]; //We will store the error here

        //First we store the buffer, then the size, the rest follow the arguements of printf (these will go in the buffer)
        snprintf(errorBuffer, sizeof(errorBuffer), "Error number: %d", errno);
        perror(errorBuffer); //We print the buffer since it contains it the error number
        return EXIT_FAILURE;
    }

    return 0;
}