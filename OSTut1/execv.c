#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main (){
    printf("Executing top -d -2 -n 10 by execvp");


    //Note the file location may be found by typing whereis top in the terminal
    char *args[] = {"/usr/bin/top", "-d", "2", "-n", "10", NULL}; //Note how we finish with NULL

    //Note firstly we point out the filename by entering the 0 index (this is where it is found in the array)
    //The we enter args so we get the whole command
    if(execvp(args[0], args) == -1){
        perror("Unable to execute execvp");
        return EXIT_FAILURE;
    }

    //This part should never be reached
    printf("Successful");
    return EXIT_SUCCESS;
}