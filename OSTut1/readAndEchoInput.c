#include <stdio.h>
#include <stdlib.h>
#include "linenoise.h"

int main(void) {

    // Get input and echo here (echo means display to terminal)
    char *line;

    /* The high level function that is the main API of the linenoise library.
 * This function checks if the terminal has basic capabilities, just checking
 * for a blacklist of stupid terminals, and later either calls the line
 * editing function or uses dummy fgets() so that you will be able to type
 * something even in the most desperate of the conditions. */
    while((line = linenoise("hello> ")) != NULL) {
        printf("%s\n", line);

        // It is safer to free memory using linenoise,
        // since memory was allocated by linenoise.
        linenoiseFree(line);
    }

    //Press control C to exit
    printf("Exiting\n");
    return EXIT_SUCCESS;
}