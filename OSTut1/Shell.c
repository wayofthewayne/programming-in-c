#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

#include "linenoise.h"

// Must be greater than 0
#define MAX_ARGS 16

// Must be greater than 2
#define MAX_NAME 32

// Exit code to use when execvp fails
#define CUSTOM_EXIT_CODE 4


//Make the child execute a command, for which the parents waits execution for
bool fork_and_exec(char *const *args) {
    // Basic error checking
    if (args == NULL || *args == NULL)
        return false;

    // Fork and exec
    pid_t pid_result = fork();
    if (pid_result == -1) {
        perror("fork failed");
        return false;
    }

    if (pid_result == 0) {
        // This is child - call execvp
        if (execvp(args[0], args) == -1) {
            perror("execvp failed");
            exit(CUSTOM_EXIT_CODE); //We use the custom exit code
        }
    } else {
        // This is parent
        int status;

        if (waitpid(pid_result, &status, 0) == -1) {
            perror("waitpid failed");
            return false;
        }

        // execvp failed (using custom error code 4)
        //If there was a problem with child, status will be 4
        if (WIFEXITED(status) && WEXITSTATUS(status) == CUSTOM_EXIT_CODE) {
            return false;
        }
    }

    return true;
}

int main(void) {
    char *line;
    const char *delimiter = " ";
    char *args[MAX_ARGS];
    char shell_name[MAX_NAME] = "myshell> ";

    while((line = linenoise(shell_name)) != NULL) {
        // Tokenise
        char *token = strtok(line, delimiter);

        // Check if user wants to exit
        if (token != NULL && strcmp(token, "exit") == 0)
            return EXIT_SUCCESS;

        // Parse and copy tokens
        int i = 0;
        while(token != NULL && i < MAX_ARGS - 1) {
            args[i++] = token;
            token = strtok(NULL, delimiter);
        }

        // Argument arrays need to be null terminated
        args[i] = NULL;

        // Employ the fork+exec pattern here and update shell name
        if (i > 0 && fork_and_exec(args)) {

            //Setting args[0] as the shell name
            int n = snprintf(shell_name, MAX_NAME, "%s> ", args[0]);

            if (n >= MAX_NAME) //If name to big
                snprintf(shell_name + (MAX_NAME - 3), 3, "> ");
        }

        // Free the string allocated by linenoise
        linenoiseFree(line);
    }

    return EXIT_SUCCESS;
}