#include <stdio.h>
#include <stdlib.h>

static void Exit_Handler1(void);
static void Exit_Handler2(void);
static void Exit_Handler3(void);

int main (void){
    if(atexit(Exit_Handler1) != 0)
        perror("Cannot register exit handler 1");

    if(atexit(Exit_Handler2) != 0)
        perror("Cannot register exit handler 2");

    if(atexit(Exit_Handler3) != 0)
        perror("Cannot register exit handler 3");



    printf("Main is finished!\n");
    return 0;
}

static void Exit_Handler1 (void){//By making it static it can only be accessed in this file
 printf("Exit_Handler1 called from function\n");
}

static void Exit_Handler2(void){
    printf("Exit_Handler2 has been called\n");
}

static void Exit_Handler3(void){
    printf("Be informed that Exit_Handler3 has been invoked\n");
}