#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "utils.h"

int main(int argc, const char **argv) {
    // Expecting only one argument (i.e the entered number)
    if (argc > 2) {
        fprintf(stderr, "Only one argument should be passed\n");
        return EXIT_FAILURE;
    }

    // Get number from argv and validate it
    int n;
    //Note we get the number and store it in n
    //Note how we use argv[1] since argv[0] is the program name
    if (!get_number(argv[1], &n) || n < 1 || n > 10) {
        fprintf(stderr, "Provided argument is not a number, or it is not in range: 1 < n < 10\n");
        return EXIT_FAILURE;
    }

    // Fork n times. In this case, child will always die.
    // Alternatively, we could choose to exit from the parent instead.
    for (int i = 0; i < n; ++i) {
        // Fork
        pid_t pid_result = fork();

        //Checking for forking errors
        if (pid_result == -1) {
            perror("fork failed");
            return EXIT_FAILURE;
        }

        // Print parent pid and the pid of this process and pid from fork
        printf("i: %d, ppid: %d, pid: %d, cpid: %d\n", i, getppid(), getpid(), pid_result);

        // If this is the child, exit (note fork returns 0 for child)
        if (pid_result == 0)
            return EXIT_SUCCESS;
    }

    printf("Parent exiting\n");

    // Note: Observe that if the Parent exits before the children,
    // the children become orphans and are re-parented to the
    // init process (systemd, etc). This is reflected in ppid printed by such children.
    // Verify this by executing: ps -p <ppid> -o comm=

    return EXIT_SUCCESS;
}
