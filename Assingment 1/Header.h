//
// Created by Wayne Falzon on 12/12/2019.
//

#ifndef ASSINGMENT_1_HEADER_H
#define ASSINGMENT_1_HEADER_H

#include <stdio.h>
#include <string.h> // for strcpy
#include <stdlib.h> // for rand()
#include <stdbool.h> // for bool type

#define NUM 10 //amount of numbers
#define LEN 6 // string length

//ANSI C Prototypes for task 1a
void generate(int *p, int x);
void shuffle(int *ar);
void sort(int *ar);
void shoot(int *ar, bool *used, int *target);
void target(int *target);
void print(int ar[]);

//ANSI C Prototypes for task 1b
void Sgenerate(char Snum[][LEN], int x, int num[]);
void Sshuffle(char (*Snum)[LEN], int num[]);
void Ssort(char (*Snum)[LEN], int num[]);
void Sshoot(char (*Snum)[LEN], bool *used, char sTarget[], int num[]);
void Starget(char sTarget[], bool *sUsed);
void Sprint(char Snum[][LEN]);

//ANSI C Prototypes for task 1c
void MainMenu();
void integerMenu();
void stringMenu();

#endif //ASSINGMENT_1_HEADER_H
