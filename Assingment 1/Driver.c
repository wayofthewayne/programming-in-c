#include "Header.h"

int main(void){

    int mainOption, intOption, stringOption;
    int scanRetMain, scanRetInt, scanRetString;

    //variables for task 1a
    int i = rand() % ((100 + 1 -1) + 1); // Generating a random number in the range 1-100
    int array[NUM]; //  An array of 10 integers
    bool used = false;
    int targetNum = 0;

    //variables for task 1b
    char Snumbers[NUM][LEN]; //Containing the string representations of the integers from 1 - 10
    int numbers[NUM]; //Will contain the respective integers
    bool sUsed = false;
    char sTargetNum[LEN];

    do {
        MainMenu();

        do { // validation of input
            printf("Please enter your option: ");
            scanRetMain = scanf("%d", &mainOption);
            while(getchar() != '\n'); //Clearing the buffer

            if(scanRetMain == 0){
                printf("Menu works on integer values.\n");
            }
        }while(scanRetMain == 0);

        switch (mainOption) {
            case 1:

                do {
                    integerMenu();

                    do { // valid for user input
                        printf("Please enter your option: \n");
                        scanRetInt = scanf("%d", &intOption);
                        while (getchar() != '\n');//Clearing buffer

                        if (scanRetInt == 0) {
                            printf("Menu works on integer values.\n");
                        }
                    } while (scanRetInt == 0);

                    switch (intOption) {
                        case 1:
                               generate(array,i);
                            break;
                        case 2:
                            shuffle(array);
                            break;
                        case 3:
                            sort(array);
                            break;
                        case 4:
                            shoot(array,&used,&targetNum);
                            break;
                        case 5:
                            target(&targetNum);
                            break;
                        case 6:
                            print(array);
                            break;
                        case 7:
                            break; // will return to outer menu
                        default:
                            printf("\nPlease enter a valid option\n\n");
                            break;
                    }
                }while(intOption != 7);
                break;

            case 2:

                do {
                    stringMenu();

                    do { // valid for user input
                        printf("Please enter your option: \n");
                        scanRetString = scanf("%d", &stringOption);
                        while (getchar() != '\n');//Clearing buffer

                        if (scanRetString == 0) {
                            printf("Menu works on integer values.\n");
                        }
                    } while (scanRetString == 0);

                    switch (stringOption) {
                        case 1:
                            Sgenerate(Snumbers, NUM, numbers);
                            break;
                        case 2:
                            Sshuffle(Snumbers, numbers);
                            break;
                        case 3:
                            Ssort(Snumbers, numbers);
                            break;
                        case 4:
                            Sshoot(Snumbers, &sUsed, sTargetNum, numbers);
                            break;
                        case 5:
                            Starget(sTargetNum, &sUsed);
                            break;
                        case 6:
                            Sprint(Snumbers);
                            break;
                        case 7:
                            break;
                        default:
                            printf("\nPlease enter a valid option!\n\n");
                            break;
                    }
                }while(stringOption != 7);
                break;

            case 3:
                exit(0);

            default:
                printf("\nPlease enter a valid option!\n\n");
                break;
        }
    }while(mainOption != 3);
    return 0;
}