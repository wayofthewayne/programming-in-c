#include "Header.h"

//TASK A FUNCTIONS
void generate(int *p, int x){ //Populates an array with integers
    for(int i=0; i<NUM; ++i)
        p[i] = (i+x);
}

void shuffle(int *ar){ //shuffles the contents of an array

    int temp=0;
    int j;

    for(int i=0; i<NUM; ++i){

        j = rand() % (i + 1); // generating a random number in the range of 0 to i

        //Swapping the elements inside the array
        temp = ar[i];
        ar[i] = ar[j];
        ar[j] = temp;
    }
}

void sort(int *ar){ //A bubble sort algorithm implementation
    int temp =0;
    bool swap = false;

    do {
        swap = false;
        for (int i = 0; i <NUM; ++i) {

            if (i != (NUM-1) && ar[i] > ar[i + 1]) { // Swapping numbers when the number before is bigger than the one after
                //Also making sure not to point at an index outside the array bounds
                temp = ar[i];
                ar[i] = ar[i + 1];
                ar[i + 1] = temp;
                swap = true;
            }
        }
    }while(swap == true); // A necessary condition to make sure there is a pass with no swaps (ie all elements sorted)
}

void shoot(int *ar, bool *used, int *target){ // Will zero out an element of the array at random
    int j;

    if(*used == false){ //To prevent the function being called twice
        j = rand() % ((NUM-1)+1); //Generating a random number from 0-9

        *target = ar[j]; // to be stored for the target function

        ar[j] = 0;
        *used = true;

    }else {
        //If called twice error messages is displayed
        printf("You have already made use of this function\n");
        *used = true;
    }
}

void target(int *target){ // Will show the value set to 0 by shoot() function
    if(*target != 0) {
        printf("You have zeroed out: %d\n", *target);
    }else{
        printf("You have yet to zero out an element\n");
    }
}

void print(int ar[]){
    printf("\n");

    for(int i=0; i<NUM; ++i)
        printf("%d\n", ar[i]);

    printf("\n");
}

//TASK B FUNCTIONS
char *convert(int no){ //function returns a pointer to an character since we are printing an array of characters
    switch (no){       // by using a pointer we return the beginning address of this string
        case 0:  //This function is responsible for converting integers into strings
            return "zero\0";
        case 1:
            return "one\0";
        case 2:
            return "two\0";
        case 3:
            return "three\0";
        case 4:
            return "four\0";
        case 5:
            return "five\0";
        case 6:
            return "six\0";
        case 7:
            return "seven\0";
        case 8:
            return "eight\0";
        case 9:
            return "nine\0";
        case 10:
            return "ten\0";
    }
}

void Sgenerate(char Snum[][LEN], int x, int num[]){ //Will populate an array with strings from "one" to "ten"

    for(int i=0; i<x; ++i){
        num[i] = (i+1); //filling an array with integers

        char *numToString = convert(i+1); //apply the convert function for the numbers
        strcpy(Snum[i], numToString); // storing the string in the array
    }
}

void Sshuffle(char (*Snum)[LEN], int num[]){ //Shuffle the string contents of the array
    int temp =0;
    int j;
    char stemp[LEN] = {'\0'};

    for(int i=0; i<NUM; ++i){
        j = rand() % (i + 1); //random number generation between i and 0 (to stay between array bounds)

        //swapping or shuffling
        temp = num[i];//Performing swap for integers
        num[i] = num[j];
        num[j] = temp;

        strcpy(stemp, Snum[i]);//Performing swap for strings
        strcpy(Snum[i],Snum[j]);
        strcpy(Snum[j], stemp);
    }

}

void Ssort(char (*Snum)[LEN], int num[]){ //Sorts the strings in the array (numerically)
    bool swap = false;
    int temp = 0;
    char stemp[LEN];

    do{
        swap = false;
        for(int i=0; i<NUM; ++i){
            if(i != (NUM-1) && num[i] > num[i+1]){ //The necessary condition to swap array contents
                // performing the swapping
                temp = num[i];//Swapping ints
                num[i] = num[i+1];
                num[i+1] = temp;

                strcpy(stemp, Snum[i]);//Performing swap for strings
                strcpy(Snum[i],Snum[i+1]);
                strcpy(Snum[i+1], stemp);
                swap = true;
            }
        }
    }while (swap == true); //To make sure there is a pass with no swaps
}

void Sshoot(char (*Snum)[LEN], bool *used, char sTarget[], int num[]){ // Will change an the content of an element of an array to "zero"
    int j;

    if(*used == false){ //Preventing from using the function more than once
        j = rand() % ((NUM-1)+1); //Generating a random number from 0-9

        strcpy(sTarget, Snum[j]);//storing the string to be used for the target function

        strcpy(Snum[j], convert(0)); //Setting the "zero"
        num[j] = 0;

        *used = true;
    }else{
        //displaying the error message if used more than once
        printf("You have already made use of this function\n");
        *used = true;
    }
}

void Starget(char sTarget[], bool *sUsed){//Will retrieve the actual value which was set to zero by the Sshoot() function
    if(*sUsed == true){
        printf("The number which was targeted was %s\n", sTarget);
    }else{
        printf("You have yet to zero out an element\n");
    }
}

void Sprint(char Snum[][LEN]){
    printf("\n");
    for(int i=0; i<NUM; ++i) {
        printf("%s\n", Snum[i]);
    }
    printf("\n");
}


//TASK C FUNCTIONS
void MainMenu(){ // Will display the main menu
    printf("******************************\n");
    printf("1. Task a (integer functions)\n");
    printf("2. Task b (string functions)\n");
    printf("3. Exit\n");
    printf("******************************\n");
}

void integerMenu(){ //Task a menu
    printf("*****************\n");
    printf("1. Generate Array\n");
    printf("2. Shuffle Array\n");
    printf("3. Sort Array\n");
    printf("4. Shoot Array\n");
    printf("5. Target Array\n");
    printf("6. Print Array\n");
    printf("7. Exit\n");
    printf("*****************\n");
}

void stringMenu(){ //Task b menu
    printf("************************\n");
    printf("1. Generate String Array\n");
    printf("2. Shuffle String Array\n");
    printf("3. Sort String Array\n");
    printf("4. Shoot String Array\n");
    printf("5. Target String Array\n");
    printf("6. Print String Array\n");
    printf("7. Exit\n");
    printf("************************\n");
}