#include <stdio.h>
#include <string.h>
#include "bookdb.h"

struct stringRecord
{
    char *str;
    long offset;
};

struct floatRecord
{
    float f;
    long offset;
};

void DrawHorizontalLine();
void DisplayBookDatabase(struct book *db, int numBooks);
void DisplayBook(struct book *book);
void DisplayBooksByTitleAscending(FILE *db, struct stringRecord *titleIndex, int numBooks);
void DisplayBooksByTitleDescending(FILE *db, struct stringRecord *titleIndex, int numBooks);
void DisplayBooksByAuthorAscending(FILE *db, struct stringRecord *authorIndex, int numBooks);
void DisplayBooksByAuthorDescending(FILE *db, struct stringRecord *authorIndex, int numBooks);
void DisplayBooksByValueAscending(FILE *db, struct floatRecord *valueIndex, int numBooks);
void DisplayBooksByValueDescending(FILE *db, struct floatRecord *valueIndex, int numBooks);

int PopulateBookDatabase(struct book *bookDb);

void SortStringRecords(struct stringRecord *records, int numRecords);
void SortFloatRecords(struct floatRecord *records, int numRecords);

int main(void)
{
    struct book bookDb[MAXBKS];
    struct stringRecord titleIndex[MAXBKS];
    struct stringRecord authorIndex[MAXBKS];
    struct floatRecord valueIndex[MAXBKS];

    // Read book.dat into bookDb.
    int numBooks = PopulateBookDatabase(bookDb);

    DisplayBookDatabase(bookDb, numBooks);

    // Initialise the title, author and value indices (unsorted).
    for (int i = 0; i < numBooks; ++i) {
        titleIndex[i].str = bookDb[i].title;
        titleIndex[i].offset = i * sizeof(struct book);

        authorIndex[i].str = bookDb[i].author;
        authorIndex[i].offset = i * sizeof(struct book);

        valueIndex[i].f = bookDb[i].value;
        valueIndex[i].offset = i * sizeof(struct book);
    }

    // Sort indices.
    SortStringRecords(titleIndex, numBooks);
    SortStringRecords(authorIndex, numBooks);
    SortFloatRecords(valueIndex, numBooks);

    FILE *db = fopen("book.dat", "rb");

    DisplayBooksByTitleAscending(db, titleIndex, numBooks);
    DisplayBooksByTitleDescending(db, titleIndex, numBooks);
    DisplayBooksByAuthorAscending(db, authorIndex, numBooks);
    DisplayBooksByAuthorDescending(db, authorIndex, numBooks);
    DisplayBooksByValueAscending(db, valueIndex, numBooks);
    DisplayBooksByValueDescending(db, valueIndex, numBooks);

    fclose(db);

    return 0;
}

void DrawHorizontalLine()
{
    printf("+--------+-");
    for (int i = 0; i < 40; ++i)
        printf("-");
    printf("-+-");
    for (int i = 0; i < 40; ++i)
        printf("-");
    printf("-+-------+\n");
}

void DisplayBookDatabase(struct book *db, int numBooks)
{
    DrawHorizontalLine();
    printf("| Offset | %-40s | %-40s | Value |\n", "Title", "Author");
    DrawHorizontalLine();

    for (int i = 0; i < numBooks; ++i) {
        printf("| %6ld | %-40s | %-40s | %5.2f |\n", i * sizeof(struct book),
               db[i].title, db[i].author, db[i].value);
    }

    DrawHorizontalLine();
    printf("\n");
}

void DisplayBook(struct book *book)
{
    printf("%-40s %-40s %5.2f\n", book->title, book->author, book->value);
}

void DisplayBooksByTitleAscending(FILE *db, struct stringRecord *titleIndex, int numBooks)
{
    printf("Books sorted by title (ascending)\n");
    printf("---------------------------------\n");

    for (int i = 0; i < numBooks; ++i) {
        fseek(db, titleIndex[i].offset, SEEK_SET);
        struct book book;
        fread(&book, 1, sizeof(struct book), db);
        DisplayBook(&book);
    }

    printf("\n");
}

void DisplayBooksByTitleDescending(FILE *db, struct stringRecord *titleIndex, int numBooks)
{
    printf("Books sorted by title (descending)\n");
    printf("----------------------------------\n");

    for (int i = numBooks - 1; i >= 0; --i) {
        fseek(db, titleIndex[i].offset, SEEK_SET);
        struct book book;
        fread(&book, 1, sizeof(struct book), db);
        DisplayBook(&book);
    }

    printf("\n");
}

void DisplayBooksByAuthorAscending(FILE *db, struct stringRecord *authorIndex, int numBooks)
{
    printf("Books sorted by author (ascending)\n");
    printf("----------------------------------\n");

    for (int i = 0; i < numBooks; ++i) {
        fseek(db, authorIndex[i].offset, SEEK_SET);
        struct book book;
        fread(&book, 1, sizeof(struct book), db);
        DisplayBook(&book);
    }

    printf("\n");
}

void DisplayBooksByAuthorDescending(FILE *db, struct stringRecord *authorIndex, int numBooks)
{
    printf("Books sorted by author (descending)\n");
    printf("-----------------------------------\n");

    for (int i = numBooks - 1; i >= 0; --i) {
        fseek(db, authorIndex[i].offset, SEEK_SET);
        struct book book;
        fread(&book, 1, sizeof(struct book), db);
        DisplayBook(&book);
    }

    printf("\n");
}

void DisplayBooksByValueAscending(FILE *db, struct floatRecord *valueIndex, int numBooks) {
    printf("Books sorted by value (ascending)\n");
    printf("---------------------------------\n");

    for (int i = 0; i < numBooks; ++i) {
        fseek(db, valueIndex[i].offset, SEEK_SET);
        struct book book;
        fread(&book, 1, sizeof(struct book), db);
        DisplayBook(&book);
    }

    printf("\n");
}

void DisplayBooksByValueDescending(FILE *db, struct floatRecord *valueIndex, int numBooks)
{
    printf("Books sorted by value (descending)\n");
    printf("----------------------------------\n");

    for (int i = numBooks - 1; i >= 0; --i) {
        fseek(db, valueIndex[i].offset, SEEK_SET);
        struct book book;
        fread(&book, 1, sizeof(struct book), db);
        DisplayBook(&book);
    }

    printf("\n");
}

int PopulateBookDatabase(struct book *bookDb)
{
    FILE *db = fopen("book.dat", "rb");
    int index = 0;

    for (;;) {
        size_t ret = fread(&bookDb[index], 1, sizeof(struct book), db);
        if (ret == 0)
            break;
        index++;
    }

    fclose(db);

    return index;
}

void SortStringRecords(struct stringRecord *records, int numRecords)
{
    // Adapted from stsrt() from Listing 11.29 by Mark Vella.

    struct stringRecord temp;
    int top, seek;

    for (top = 0; top < numRecords - 1; top++) {
        for (seek = top + 1; seek < numRecords; seek++) {
            if (strcmp(records[top].str, records[seek].str) > 0) {
                temp = records[top];
                records[top] = records[seek];
                records[seek] = temp;
            }
        }
    }
}

void SortFloatRecords(struct floatRecord *records, int numRecords)
{
    struct floatRecord temp;
    int top, seek;

    for (top = 0; top < numRecords - 1; top++) {
        for (seek = top + 1; seek < numRecords; seek++) {
            if (records[top].f > records[seek].f) {
                temp = records[top];
                records[top] = records[seek];
                records[seek] = temp;
            }
        }
    }
}
