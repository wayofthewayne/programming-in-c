#include <stdio.h>
#include <stdlib.h>
#include <errno.h> //To get specific error message
#include <assert.h> // for assert
#include <string.h> // for strlen

#define INPUT_FILE "test.txt"

long GetFileSize(FILE *f);
char *ReadFileToString(char *fileName, char *mode);

int main(void){

    char *inputString = ReadFileToString(INPUT_FILE, "rb");  //read in binary mode
    if(inputString == NULL){
        exit(EXIT_FAILURE);
    }
    size_t lenInputStr = strlen(inputString);

    FILE *fp = fopen(INPUT_FILE, "wb"); //write in binary mode
    if(fp == NULL){
        printf("fopen() failed with error %d\n", errno);
        free(inputString);
        exit(EXIT_FAILURE);
    }

    char *offset = inputString;//starting the search and replace
    char oldWord[] = {"wayne"};
    char newWord[] = {"ben"};

    size_t lenOldWord = strlen(oldWord);
    size_t lenNewWord = strlen(newWord);

    for(;;){ //infinite loop
        char *found = strstr(offset, oldWord);
        if(found == NULL){
            size_t processed = offset - inputString; // processed bytes
            size_t remaining = lenInputStr - processed; // remaining bytes
            if(fwrite(offset, 1, remaining,fp) != remaining)
                printf("fwrite() failed with error %d", errno);
            break;
        }
        size_t bytesToWrite = (size_t) (found - offset);
        size_t bytesWritten = fwrite(offset, 1, bytesToWrite, fp);  // copying till the found word

        if(bytesWritten != bytesToWrite){
            printf("fwrite() failed with error %d\n", errno);
            break; //break and would close
        }

        offset += bytesToWrite;

        bytesWritten = fwrite(newWord, 1, lenNewWord, fp); // writing the new word
        if(bytesWritten != lenNewWord){
            printf("fwrite() failed with error %d\n", errno);
            break; //break and would close
        }
        offset += lenOldWord; //updating offset;
    }



    fclose(fp);
    free(inputString);

    return 0;
}

long GetFileSize(FILE *f){
    int ret = fseek(f, 0L, SEEK_END); //going to end of file

    if(ret != 0){
        printf("fseek() failed with error %d\n", errno);
        return -1;
    }

    long fileSize = ftell(f); //Getting size of file
    if(fileSize == -1){
        printf("ftell() failed with error %d\n", errno);
        return -1;
    }

    return fileSize;
}

char *ReadFileToString(char *fileName, char *mode){

    FILE *fp = fopen(fileName, mode);

    if(fp == NULL){
        printf("fopen() failed with error %d\n", errno);
        return NULL;
    }

    long fileSize = GetFileSize(fp);
    if(fileSize == -1){
        fclose(fp); // File would be open by now so we close
        return NULL;
    }
    char *inputString = malloc(fileSize +1); // Allocating memory +1(for null terminator)
    if(inputString == NULL){
        printf("malloc() fail!");
        fclose(fp);
        return NULL;
    }

    rewind(fp); //goes to the beginning of the file

    //size_t is just unsigned long using typedef to be the same on all systems in terms of bytes
    size_t numElementsRead = fread(inputString, 1, fileSize, fp); // will read the file into inputString
    assert(numElementsRead == fileSize); // to make sure all bytes in file are read

    inputString[fileSize] = '\0'; //allocating null terminator (note it is zero based index)

    fclose(fp);

    return inputString;
}