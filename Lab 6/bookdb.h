#pragma once

#define MAXTITL 40
#define MAXAUTL 40
#define MAXBKS  20 /* maximum number of books */

#include <string.h>

struct book /* set up book template */
{
    char title[MAXTITL];
    char author[MAXAUTL];
    float value;
};

void CreateBookDatabase(void);
char *sgets(char *str, int size);

