#include <stdio.h>
#include <ctype.h> // for isUpper and isDigit
#include <stdbool.h> // for boolean


int main (void){

     int ch; // we use int so it can return -1 (not a char) ie the eof command
     int wordLen = 0;
     bool hypenFound = false;
     int condition1Counter = 0;
     bool upperFound = false;
     int condition2Counter = 0;
     int condition3Counter = 0;


while((ch = getchar()) != EOF) {
    if (isspace(ch) || ch == ',' || ch == '!' || ch == '.') {


        if (wordLen > 10) { // for words longer than 10 characters that don't contain a hypen
            if (!hypenFound)
                condition1Counter++;
        }

        upperFound = false;
        hypenFound = false;
        wordLen = 0;

    } else {

        if (ch == '-')
            hypenFound = true;


    }

    if(wordLen > 1 &&  isupper(ch) && !upperFound){ // Finding upper case characters
         condition2Counter++;
         upperFound = true; // Once the boolean is satisfied it won't re-enter the loop (WAYNE) counts as 1 error not 4
    }

    if(wordLen == 1 && isdigit(ch)){ //Checking if the first letter of a word is a digit
        condition3Counter++; 
    }


 wordLen++;
}

int errors;

errors = condition1Counter + condition2Counter + condition3Counter;

    printf("Words longer than 10 characters and do not contain a hyphen: %d\n", condition1Counter);
    printf("Words with upper case characters (excluding the first one): %d\n", condition2Counter);
    printf("Words with starting non-alphabetic characters: %d\n", condition3Counter);
    printf("Total errors: %d\n", errors);
    return 0;
}