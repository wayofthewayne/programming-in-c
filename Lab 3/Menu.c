#include <stdio.h>
#include <stdlib.h> // for exit

#define NUMBEROFITEMS 6


int main (void){

    int option, shopOption, quantity[NUMBEROFITEMS]= {0,0,0,0,0,0}, addMore, newSession, exitOption, code[NUMBEROFITEMS];
    float price[NUMBEROFITEMS] = {0.5, 3.25, 5.60, 4.35, 1.25, 5.20};
    float totalPerItem[NUMBEROFITEMS] = {0.0,0.0,0.0,0.0,0.0,0.0};
    float total =0.0;
    char names[NUMBEROFITEMS][15] = {"Bananas", "Chocolate", "Pudding", "Potatoes", "Lighter", "File-Paper"};

    for(int i=0; i<NUMBEROFITEMS; ++i){
        code[i] = i; // setting the elements of the array equal to the code
    }

    do {

        BTM: // a label for goto

        printf("\n\n\tMenu\n"); // menu display
        printf("1. Add item\n");
        printf("2. Show current total\n");
        printf("3. Check out\n");
        printf("4. Cancel session\n");
        printf("5. Quit\n");
        printf("Choice: ");
        scanf("%d", &option);

        switch (option) {

            case 1:

                do {

                    printf("\n\n\nShopping List\n");
                    printf("%15s%15s%15s", "Code", "Item", "Price\n");
                    printf("\n");
                    for(int i=0; i<NUMBEROFITEMS; ++i){
                        printf("%15d%15s%14.2f%1s", code[i], names[i], price[i], "$\n");
                    }
                    printf("%15s%15s", "6.", "Exit\n");


                    do {
                        printf("What would you like (enter code)? \n");
                        scanf("%d", &shopOption);
                        if (shopOption < 0 || shopOption > 6) {
                            printf("Please enter a valid option\n");
                        }
                    } while (shopOption < 0 || shopOption > 6);

                    if(shopOption == 6)
                        goto BTM;

                    printf("Quantity: ");
                    scanf("%d", &quantity[shopOption]);
                    printf("\n");

                    switch (shopOption) {

                        case 0:
                            totalPerItem[0] = price[0] * quantity[0];
                            break;
                        case 1:
                            totalPerItem[1] = price[1] * quantity[1];
                            break;
                        case 2:
                            totalPerItem[2] = price[2] * quantity[2];
                            break;
                        case 3:
                            totalPerItem[3] = price[3] * quantity[3];
                            break;
                        case 4:
                            totalPerItem[4] = price[4] * quantity[4];
                            break;
                        case 5:
                            totalPerItem[5] = price[5] * quantity[5];
                            break;
                    }

                    printf("Would you like to add another item to your cart?\n");
                    printf("Press 1 to continue shopping\n");
                    printf("Press 2 to exit\n");
                    scanf("%d", &addMore);


                }while(addMore == 1);

                 break;
            case 2:
                 total=0;
                for(int i=0; i<NUMBEROFITEMS; ++i){
                        total += totalPerItem[i];
                }

                printf("\n\n\n\n\n\nYour current total: %.2f\n\n", total);
                break;
            case 3:
                printf("Your order includes: \n");
                printf("\n%15s%15s%15s%15s\n\n", "Code", "Names", "Quantity", "Total for item");

                for(int i=0; i<NUMBEROFITEMS; ++i){
                    printf("%15d%15s%15d%14.2f%1s", code[i], names[i], quantity[i], totalPerItem[i],"$\n");
                }

                total=0;
                for(int i=0; i<NUMBEROFITEMS; ++i){
                    total += totalPerItem[i];
                }

                printf("Total: %14.f", total);

                break;
            case 4:
                printf("Are you sure u want to start a new session?\n");
                printf("Press 1 if yes\n");
                printf("Press 2 if no\n");
                scanf("%d", &newSession);

                if(newSession == 1){
                    total = 0;
                    for(int i=0; i<NUMBEROFITEMS; ++i){
                        totalPerItem[i] = 0;
                        quantity[i]=0;
                    }
                }

                break;
            case 5:
                printf("Are you sure u want to exit?\n");
                printf("Press 1 if yes\n");
                printf("Press 2 if no\n");
                scanf("%d", &exitOption);

                if(exitOption == 1)
                exit(0);

                break;

            default:
                printf("Please choose a valid option!\n\n");
                break;
        }

    }while(option > 0 || option <= 5);

}
