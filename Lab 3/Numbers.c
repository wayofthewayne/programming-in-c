#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main (void) {

    int ch;
    char word[10];
    int wordCounter = 0;
    float num;
    int error = 0;

    while ((ch = getchar()) != EOF) {

        if (isspace(ch)) { // if we encounter a space the whole number can be read
            wordCounter = 0;


            num = atof(word); //changing the characters to be treated as a float

            if(num == 0.00){ //if the stream does not contain a number it will be converted to this value.
                error++;
            }


            if(num >0  && num <= 100)
               printf("%.2f\n", num);

        } else { //no spcae encountered i.e num not completely read


            word[wordCounter] = ch;
            wordCounter++;
        }
    }

    printf("A total of %d of garbage input encountered", error);
}
