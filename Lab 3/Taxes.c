#include <stdio.h>

#define BRACKET1 10000
#define BRACKET2 18000
#define GREENBRACKET1 15000
#define GREENBRACKET2 23000
#define RATE1 0.18
#define RATE2 0.2
#define RATE3 0.25
#define REBATERATE 0.05


int main (void){

    float income, tax1 , tax2, tax3, totaltax, net, rebate, NetTax;
    int choice, IT;

    totaltax = tax1 = tax2 = tax3 = net = rebate = NetTax = 0;

    printf("Enter your income: ");
    scanf("%f", &income);

    printf("Do you collect old electric equipment for green disposal? \n" );
    printf("Press 1 if yes, Press 2 if no: \n");
    scanf("%d", &choice);

    switch(choice){

        case 1: // The green rate tax applies here

        if(income > 0 && income < GREENBRACKET1){
            tax1 = income * RATE1;
        }else if (income > GREENBRACKET1){
            tax1 = GREENBRACKET1 * RATE1;
        }

        if(income > GREENBRACKET1 && income < GREENBRACKET2){
            tax2 = (income - GREENBRACKET1) * RATE2;
        }else if (income > GREENBRACKET2){
            tax2 = (GREENBRACKET2 - GREENBRACKET1) * RATE2;
            tax3 = (income - GREENBRACKET2) * RATE3;
        }
            break;

        case 2: // normal taxing

        if(income>0 && income < BRACKET1){
            tax1 = income * RATE1;
        }else if (income > BRACKET1){
            tax1 = BRACKET1 * RATE1;
        }

        if(income > BRACKET1 && income < BRACKET2){
            tax2 = (income - BRACKET1) * RATE2;
        } else if(income > BRACKET2){
            tax2 = (BRACKET2 - BRACKET1) * RATE2;
            tax3 = (income - BRACKET2) * RATE3;
        }
            break;

        default:
            printf("Invalid option");
            break;
    }

    printf("Do you work in the IT industry?\n");
    printf("If yes press 1, if no press 2\n");
    scanf("%d", &IT);

    totaltax = tax1 + tax2 + tax3;

    if(IT == 1){
        rebate =  totaltax * REBATERATE;
    }

    NetTax = totaltax - rebate;

    net = income - NetTax;

    printf("\n\nNET: %.2f", net);
}
