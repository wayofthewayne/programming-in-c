#include <stdio.h>
#include <ctype.h> // for isUpper and isDigit
#include <stdbool.h> // for boolean


int main (void){

    int ch; // we use int so it can return -1 (not a char) ie the eof command
    int wordLen = 0;
    bool hypenFound = false;
    int condition1Counter = 0;
    bool upperFound = false;
    int condition2Counter = 0;
    int condition3Counter = 0;
    char previousChar = 0;

    //variables used for program in 8.
    int error1=0;
    int error2=0;

    while((ch = getchar()) != EOF) {

        if(wordLen > 1 &&  isupper(ch) && !upperFound){ // Finding upper case characters
            condition2Counter++;
            upperFound = true; // Once the boolean is satisfied it won't re-enter the loop (WAYNE) counts as 1 error not 4

        }


        if(wordLen == 1 && isdigit(ch)){ //Displaying an error when first letter of a word is a digit
            condition3Counter++;
        }


        if(previousChar == ' ' && ispunct(ch)) //checking for punctuation preceded by a space
            error1++;

        if(previousChar == ' ' && ch == ' '){
            error2++;
        }


        if (isspace(ch) || ch == ',' || ch == '!' || ch == '.') { // if it is end of word



            if (wordLen > 10) { // for words longer than 10 characters that don't contain a hyphen
                if (!hypenFound) {
                    condition1Counter++;
                }


            }

            upperFound = false;
            hypenFound = false;
            wordLen = 0;

        } else {

            if (ch == '-')
                hypenFound = true;

        }



        wordLen++;
        previousChar = ch;




    }

    int errors;

    errors = condition1Counter + condition2Counter + condition3Counter;

    printf("\n\nProgram 7 conditions: \n");
    printf("Words longer than 10 characters and do not contain a hyphen: %d\n", condition1Counter);
    printf("Words with upper case characters (excluding the first one): %d\n", condition2Counter);
    printf("Words with starting non-alphabetic characters: %d\n", condition3Counter);
    printf("Total errors: %d\n\n", errors);
    printf("Program 8 conditions: \n");
    printf("Punctuations preceded by a space: %d\n", error1);
    printf("Double spaces: %d\n", error2);
    return 0;
}
