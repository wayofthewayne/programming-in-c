#include <stdio.h>

int main (void){

    int  factorial = 1;
    int n;

    printf("Which factorial would you like to compute\n");
    scanf("%d", &n);

    for(int i=1; i<=n; ++i){

        factorial = factorial * i;

    }

    printf("The factorial is: %d ", factorial);
}

