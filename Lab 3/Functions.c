#include <stdio.h>
#include <stdbool.h>

#define MAX_INPUTS 7

int main(void){

   double input[MAX_INPUTS];
   int numInputs = 0;
   double currentInput;
   double currentMax = 0;
   double average, mode =0, median ;
   bool isNonUnique;
   double temp, temp1=0;


   while(numInputs < MAX_INPUTS){
       printf("Enter Number (-1 to quit): ");
       scanf("%lf", &currentInput);


       if(currentInput == -1) // For user to quit
           break;

       if(currentInput < 1 || currentInput > 100){ // Validation between one 1 - 100
           printf("Number needs to be between 1 and 100\n");
           continue;
       }

       if(currentInput < currentMax){ //Checking weather numbers are entered in ascending order
           printf("Number need to be in ascending order\n");
           continue;
       }

       if(currentInput == currentMax)  //Determining that we have at least one duplicate number
           isNonUnique = true;

       currentMax = currentInput;

       input[numInputs] = currentInput;
       temp1 +=  input[numInputs];
       numInputs++;

       if(numInputs == MAX_INPUTS -1 &&  !isNonUnique){ // if the user enters all the numbers but the last and there is not one duplicate we duplicate the last
           input[numInputs] = currentMax;
           temp1 += input[numInputs];
           numInputs++;

       }

   }



   if(!isNonUnique)
      printf("No duplicate values\n");

   if((sizeof(input)/8)%2==0){ // size of arr is divided by 8 because of double takes twice the amount of bytes
       median = ((input[(sizeof(input)/8)/2]+input[((sizeof(input)/8)/2-1)])/2); // taking the average of the two middle most elements in the case of even inputs
   }else{
       median = input[(sizeof(input)/8)/2]; // finding the middle most element in odd number of inputs
   }

   average = temp1 / numInputs;

//working for the mode
   int commonNum= 0;

       for(int i=0; i<(numInputs); i++){

           if(input[i] == input[i+1]){
               commonNum++;
           }else if(i != (numInputs-1) && (input[i] != input[i+1] ) && commonNum > mode){
               mode = input[i];
               commonNum =0;
           }else if(i == (numInputs -1) && commonNum > mode ){
               mode = input[i];
           }
       }

   printf("\n\nNumber of inputs: %d\n", numInputs);
   printf("Average: %lf\t", average);
   printf("Median: %lf\t", median);
   printf("Mode: %lf\t", mode);


   return 0;
}