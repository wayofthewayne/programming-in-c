/* booksave.c -- saves structure contents in a file */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define MAXTITL   40
#define MAXAUTL   40
#define MAXBKS   10             /* maximum number of books */

struct book {                   /* set up book template    */
    char title[MAXTITL];
    char author[MAXAUTL];
    float value;
};

char *sgets(char *str, int size)
{
    char *ret;
    ret = fgets(str, size, stdin);
    if(!ret)
        return ret;
    if(str[strlen(str)-1] == '\n')
    {
        str[strlen(str)-1] = '\0';
    } else {
        while(getchar() != '\n');
    }
    return ret;
}

int main(void)
{
    struct book library[MAXBKS]; /* array of structures */
    //struct book *library;
    int count = 0;
    int index, filecount;
    FILE * pbooks;
    int size = sizeof (struct book);
    int choice;


    /* do{
    printf("Would you like to add a book ? ");
    printf("Press 1 if yes, Press 2 if no");       USE SOMETHING ALONG THESE LINES TO MAKE A DYNAMIC ARRAY OF STRUCTS
    scanf("%d", &choice);                           REMEMBER TO FREE
    if(choice == 1){                                 POSSIBLY WRITE IN A FUNCTION WITH A COUNTER FOR THE NUMBER OF BOOKS TO BE CREATED
        library = realloc(library, choice * sizeof(struct book ));
    }
    }
    while(choice != 2); */

    if ((pbooks = fopen("book.dat", "a+b")) == NULL)
    {
        fputs("Can't open book.dat file\n",stderr);
        exit(1);
    }

    rewind(pbooks);            /* go to start of file     */
    while (count < MAXBKS &&  fread(&library[count], size, 1, pbooks) == 1)
    {
        if (count == 0)
            puts("Current contents of book.dat:");
        printf("%s by %s: $%.2f\n",library[count].title, library[count].author, library[count].value);
        count++;
    }
    filecount = count;
    if (count == MAXBKS)
    {
        fputs("The book.dat file is full.", stderr);
        exit(2);
    }
    puts("Please add new book titles.");
    puts("Press [enter] at the start of a line to stop.");
    while (count < MAXBKS && sgets(library[count].title, MAXTITL) != NULL && library[count].title[0] != '\0')
    {
        puts("Now enter the author.");
        sgets(library[count].author, MAXAUTL);
        puts("Now enter the value.");
        scanf("%f", &library[count++].value);
        while (getchar() != '\n')
            continue;                /* clear input line  */
        if (count < MAXBKS)

            puts("Enter the next title.");
    }
    if (count > 0)
    {
        puts("Here is the list of your books:");
        for (index = 0; index < count; index++)
            printf("%s by %s: $%.2f\n",library[index].title,
                   library[index].author, library[index].value);
        fwrite(&library[filecount], size, count - filecount, pbooks);
    }
    else
        puts("No books? Too bad.\n");
    puts("Bye.\n");
    fclose(pbooks);
    return 0;
}
