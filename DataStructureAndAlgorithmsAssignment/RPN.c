#include <stdio.h>
#include <stdlib.h>

//Defining Stack Structure
typedef struct Node{
    double data;
    struct Node * next;
}node;

typedef struct Stack{
    struct Node * top;
}stack;

//Function Prototypes

/* Function creates a stack
 * Function return a pointer to type stack
 * Function does not accept any parameters*/
stack * IntialiseStack(void);

/* Function will put a number on top of the stack
 * Function does not return anything
 * Function accepts a pointer to type stack, and a double type*/
void Push(stack * ptr, double value);

/* Function will remove a value from the top of the stack
 * Function returns the value
 * Function accepts a pointer to type stack*/
double Pop(stack * ptr);

/* Function will display the stack
 * Function does not return anything
 * Function accepts a pointer to type stack*/
void DisplayStack(stack * ptr);

/* Function converts the numbers into the expression from string to double
 * Function returns the number in double type
 * Function accepts an integer for the lenght of the number, and a pointer to the array*/
double StringToDouble(char const *array, int numberLength);

int main (){
    char expression[100]; // This array will hold the expression
    char c;
    int i=0, counter =0; // i will keep track of the chars in expression, while counter counts the chars in a number
    char number[10] = {'\0'}; //This array will store the number
    double value, temp1 , temp2;

    stack * ptr = IntialiseStack();

    //Retrieving input
    printf("Enter expression in RPN format. Please separate numbers by a space \n");
    fgets(expression, sizeof(expression), stdin);


    for(;;){
        c = expression[i];

        if(c != ' ' && c != '+' && c != '-' && c != '*' && c != '/'){
            number[counter] = expression[i]; //When it is not an operator or a space we store the char in the array
            counter++; // the counter is incremented
        }else if (c == ' ') {
            value = StringToDouble(number, counter); //The number is then converted
            counter=0;
            if( value != 0) { //If the value returned is 0, an error with conversion has happen, so we don t push
                Push(ptr, value);
                DisplayStack(ptr);
            }
        }

        if(c == '0' && counter == 1){ //Since the function does not convert to 0, we account for the special case in this manner
            //Counter needs to be 1 else the 0 in 10 would be processed as a '0'
            //To accommodate for the case of 0.5
            i++;
            if(expression[i] != '.') {
                Push(ptr, 0);
                DisplayStack(ptr);
            }
            i--;
        }

        if(c == '+' || c == '-' || c == '*' || c == '/'){ //When the number is an operator, the respective switch case
//is chosen. We pop the first two numbers on the stack. The second number is the first one entered.
//The operation is carried out, and the new value is pushed to stack.

            switch(c){
                case '+':
                    temp1 =  Pop(ptr);
                    temp2 = Pop(ptr);
                    value = temp2 + temp1;
                    Push(ptr, value);
                    DisplayStack(ptr);
                            break;

                case '-':
                    temp1 = Pop(ptr);
                    temp2 = Pop(ptr);
                    value = temp2 - temp1;
                    Push(ptr,value);
                    DisplayStack(ptr);
                    break;

                case '*':
                    temp1 = Pop(ptr);
                    temp2 = Pop(ptr);
                    value = temp2 * temp1;
                    Push(ptr, value);
                    DisplayStack(ptr);
                    break;

                case '/':
                    temp1 = Pop(ptr);
                    temp2 = Pop(ptr);
                    value = temp2 / temp1;
                    Push(ptr, value);
                    DisplayStack(ptr);

                    if (temp1 == 0){ //Dividing by 0 error
                        perror("\nDividing by 0");
                        exit(3);
                    }
                    break;

            }
        }

        i++; //i is incremented to move to the next character in the expression array.

        if(c == '\0') //If end of string is reached we break out of infinite loop
            break;
    }

    return 0;
}
//Functions

//This will create the stack
stack * IntialiseStack(void){

    stack * ptr = (stack *) malloc (sizeof(stack)); //space allocated for stack
    ptr->top = NULL; // Top pointer set to NULL (empty stack)
}

//Push an item to top of stack
void Push(stack * ptr, double value){

    node * newNode = (node *) malloc (sizeof(node));//Allocating space for node

    //Checking if memory allocation was successful
    if(ptr == NULL){
        perror("Unable to locate memory.");
        exit(1);
    }

    newNode->data = value;


    if(ptr->top == NULL){ //Stack is empty
        newNode->next = NULL; //The next node is set to NULL
    }else{
        newNode->next = ptr->top; //Next is set to the top
    }

    ptr->top = newNode; //And the top is set to the newNode
}

//Remove item from top of stack
double Pop(stack * ptr){

    double PopNumber;
    if(ptr->top == NULL){ //Checking is stack is empty
        perror("Stack is empty. Invalid use of Pop()");
        exit(2);
    }

    node *temp = ptr->top; //We set the temporary node to the top

    PopNumber = ptr->top->data; //saving the current number at the top

    ptr->top = ptr->top->next; //The top is set to the next
    free(temp); // Freeing temp (i.e the previous top is removed)

    return PopNumber;
}

//Will display the stack
void DisplayStack(stack * ptr){
    if(ptr->top == NULL){
        printf("The stack is empty");
        return;
    }

    node *temp = ptr->top; //making temp point to the top

    printf("\n Content of stack are: \n");
    while(temp->next != NULL) { //Printing all data till it s not null
        printf("%.2f\n", temp->data);
        temp = temp->next;
    }

    printf("%.2f", temp->data); //printing last element
    printf("\n");
}

//Convert a string to double.
double StringToDouble(char const *array, int numberLength){

    char storeNumber[numberLength];

    for(int i=0; i<numberLength; ++i){
        storeNumber[i] = array[i]; //The numbers are moved from the array into the another array
    }

    return atof(storeNumber);
    //The function then converts all the characters in the array to a double and returns the value
}
