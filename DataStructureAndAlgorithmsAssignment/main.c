#include <stdio.h>
#include <stdlib.h>

#define arrayA 259
#define arrayB 300

void Generate(int *ar, int size);
void QuickSort(int *ar, int leftPointer, int rightPointer);
int Partition(int *ar, int leftPointer, int rightPointer);
void ShellSort(int *ar);
void Print(int *ar, int size);

int main() {

    int arr[arrayA];
    int array[arrayB];

    printf("The first list\n");
    Generate(arr, arrayA);
    Print(arr,arrayA);

    printf("\nThe second list\n");
    Generate(array, arrayB);
    Print(array, arrayB);

    QuickSort(array,0,arrayB-1);
    ShellSort(arr);

    printf("\n\nSorted...\n\n");
    printf("First list\n");
    Print(arr, arrayA );
    printf("\nSecond list\n");
    Print(array, arrayB);



    return 0;
}

void ShellSort(int *ar){

    int interval, outerHead, innerHead, selectedValue;
    interval = 1;

    while(interval <= arrayA/3){ //Makes use of the formula until the interval is at least 1/3 of the array length
        interval = interval*3 + 1; //Kruth's formula for the interval
    }

    while(interval >0){ // The interval needs to be greater than 0

        for (outerHead = interval; outerHead < arrayA; outerHead++){ // for loop from interval value till end of array
             selectedValue = ar[outerHead]; // Acts a temporary variable for swapping
             innerHead = outerHead;

             //The first condition is needed due to the 0 based index of arrays (ie avoid segmentation faults)
             while((innerHead > interval - 1) &&  ar[innerHead - interval] >= selectedValue){ // Comparing Values at each end of gab
                 ar[innerHead] = ar[innerHead - interval]; // performing swaps when necessary
                 // note so far only 1/2 of the swap is  (i.e 1 of 2 values is changes)
                 innerHead -= interval; // shifting to the other element in current comparison
                 //Here we either check once more if there is need for another swap (ie reentering the loop),
                 //if not it is set to the value we are considering, that is the selected value in the next line.
             }

             ar[innerHead] = selectedValue; //Changing the other value (i.e the value at position innerhead - interval)
             //Note if no swap is performed, the value at position (interval - innerhead) is left as is, and on the other side
             //it is set to the selected value, that is the value we are considering
        }
        interval = (interval-1)/3; // reversing interval values, till a negative shows up to break outer loop
        //This has the effect of diving the array into smaller subn groups
    }
}

void QuickSort(int *ar, int leftPointer, int rightPointer) {

    if(leftPointer < rightPointer) { //Once this condition no longer holds the elements are sorted

        int pivot = Partition(ar, leftPointer, rightPointer); //Partition the whole array

        QuickSort(ar, leftPointer, pivot - 1); //QuickSort before the pivot
        QuickSort(ar, pivot + 1, rightPointer); //QuickSort after the pivot
    }
}

int Partition(int *ar, int leftPointer, int rightPointer){
    int pivot = ar[rightPointer]; //setting the pivot as the last element of the array
    int temp, j;
    j = (leftPointer -1); // This will be our starting index
//j acts as a temporary variable, which is swapped with numbers smaller than the pivot.
//It is incremented consistently by 1.
    for(int i = leftPointer; i<=rightPointer-1; i++){//A for loop throughout the whole array
           if(ar[i] < pivot){ //If the current position is less than the pivot
               //Performing the swap between i and j
               j++; // we increment before, since we already subtracted a 1
               temp = ar[j];
               ar[j] = ar[i];
               ar[i] = temp;
           }
           //This for loop along with the if condition will push all numbers less than the pivot towards the end of the list
    }
    //Swapping j+1 and the rightPointer. This has the effect of placing the pivot at the right point
    //This is because we swap the pivot with the next element, that was greater than the pivot.
    //We know it is greater since the swap was not performed before.
    temp = ar[j+1];
    ar[j+1] = ar[rightPointer];
    ar[rightPointer] = temp;
    return j+1; //Will return the index of the pivot, since j+1 is set to the pivot.

    //All elements before the pivot are less than the pivot.
    //All elements after the pivot are greater than the pivot.
}

void Generate(int *ar, int size) { //Populates an array with integers
    for (int i = 0; i < size; i++) {
        ar[i] = rand() % (1024); // generates random number between 0-1024
    }
}

void Print(int *ar, int size){//Will print all the array
    for(int i=0; i<size; i++){
        printf("%d\t", ar[i]);
    }
}




