#include <stdio.h>
double Absolute(double num);
double SquareRoot(double num);

int main(void){
    double number;
    printf("Enter the positive number u wish to find the square root of: ");
    scanf("%lf", &number);
    printf("\n\nThe square root of %lf is %lf", number,  SquareRoot(number));
    return 0;
}

//Although C has a standard function for this, it work only for integers.
double Absolute(double num){//This function will find the absolute value of a number.
    if (num<0){
        num = -num;
    }
    return num;
}

double SquareRoot(double num){
    double tolerance = 0.0000000000001; //This will be our degree of accuracy
    double guess = 1.0; //This will be our starting guess
    while(Absolute((guess * guess) - num) > tolerance){ //We square our guess, and reduced the number,
        //We then check if it greater than our expected degree of accuracy. This has the effect of removing the "Root"
        guess = 0.5 * (guess + num/guess); //Making use of the Algorithm
    }
    return guess; //returning approximation
}