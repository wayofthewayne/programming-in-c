#include <stdio.h>
#include <stdbool.h>
#include <math.h>

bool PrimeCheck(int n);
void SieveOfEratosthenes(int n);

int main (void){
    bool output;
    int input = 71;

    output = PrimeCheck(input);

    if(output == false){
        printf("This number is not prime i.e %d", input);
    }else{
        printf("This number is prime i.e %d", input);
    }

    printf("\n\n");
    SieveOfEratosthenes(100);
    return 0;
}

bool PrimeCheck(int n){

     if(n<1){//A check to make sure the number is greater than 1
         return false;
     }

     for(int i=2; i<=ceil(sqrt(n)); ++i){ //We may check up to square root (explain why in documentation)
         if(n%i == 0){ //The number is checked as a divisor
             return false;
         }
     }
     return true; //If the number has no divisors it must be prime
}

void SieveOfEratosthenes(int n){
    int Numbers[n];
    int currentNumber = 2;
    bool mark = false; //This act as a stopping condition for the algorithm.
    //It purpose is to indicate when a number is marked

    for(int i=0; i<n; ++i){ //Here we create a the list from 1 to n
        Numbers[i] = i+1;
    }

    do {
        for (int i = (currentNumber * currentNumber)-1; i <=n; i+=currentNumber) {
            Numbers[i] = 0; //The for loop will move in increments of currentNumber, starting from its square.
            //At these indexes the value will be set to 0. These will be the non primes.
        }

        for(int i=0; i<n+1; ++i){
            if(currentNumber < Numbers[i]){ //The next large number which was not set to zero is found
                currentNumber = Numbers[i]; //We will now move with increments of it
                mark = true; //Since there are numbers left to mark, we set it true
                break;
            }
            mark = false; //If this line is reached (at the end of the loop),
            // it implies there are no longer any numbers to mark
        }
    }while(mark == true); //We repeat until there are no longer any number unmarked

    for(int i=1; i<n; ++i){
        if(Numbers[i] != '\0')
            printf("%d\n", Numbers[i]); //The number not set to zero (the primes) are printed
    }
}

