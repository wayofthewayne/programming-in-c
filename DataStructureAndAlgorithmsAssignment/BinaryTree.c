#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#define COUNT 10


//ADT structure
typedef int Item; //This will represent a number

typedef struct treeNode{
    Item number;
    struct treeNode * left; //A pointer to the left
    struct treeNode * right; //A pointer to the right
}TNode;

typedef struct tree{
    struct treeNode * root; //A pointer to the root
    int size; //The size of the tree
}Tree;

//This struct is used by a function
typedef struct pair {
    struct treeNode *parent; //A pointer to the parent
    struct treeNode *child; //A pointer to the child
}Family;

//Function Prototypes

/* Function will initialise Tree type
 * Function will return pointer of type Tree
 * Function accepts no parameters*/
Tree* initialiseTree(void);

/* Function will determine if one should move to the left of the tree
 * Function returns a bool type depending on values
 * Function accepts two pointers to item types (numbers) */
bool ToLeft(const Item * num1, const Item * num2);

/* Function will determine if one should move to the right of the tree
 * Function returns a bool type depending on values
 * Function accepts two pointers to item types (numbers) */
bool ToRight(const Item * num1, const Item * num2);

/* Function will look for an item within the tree
 * Function returns found item
 * Function accepts a pointer to item to be found as pointer, and a pointer to a type Tree*/
Family FindItem(const Item *findMe, const Tree *ptree);

/* Function adds a Node to the tree
 * Function returns nothing
 * Function accepts two pointers to TNode, where one is the new node being added and other is the root*/
void AddNode(TNode *newNode, TNode *root);

/* Function will attempt to add an Item to the tree
 * Function returns a bool determining if the adding was successful
 * Function accepts a pointer to a Item and a pointer to a Tree*/
bool AddItem(const Item *num, Tree * ptr);

/* Function will display the whole tree
 * Function does not return anything
 * Function accepts a pointer to a tree node, and an integer*/
void DisplayTree(TNode *ptr, int space);

//Driver
int main (){
    Tree * ptr;
    int number;
    bool addMore;
    char entry;
    ptr = initialiseTree();


    do {
        printf("Please enter a value to be placed in the tree.\n");
        scanf("%d", &number);

        addMore = AddItem(&number, ptr);
        while(getchar() != '\n'); //buffer clear

        DisplayTree(ptr->root, 0);

        printf("Would you like to continue ? (y/n)\n");
        scanf("%c", &entry);
        while(getchar() != '\n'); //buffer clear

    }while(addMore == true && entry == 'y');

    return 0;
}

//All Functions

//This will create our defined ADT, return a pointer to the ADT
Tree* initialiseTree(void){
     Tree *ptree = (Tree *) malloc(sizeof(Tree)); //Allocating space for ADT

     if(ptree == NULL){//Checking memory allocation was successful
         printf("\nUnable to allocate memory\n");
         exit(3);
     }

     ptree->size = 0; //Setting the size of the tree to zero
     ptree->root = NULL; //Setting the root to NULL

     return ptree;
}

//Will determine whether the value goes to the left
bool ToLeft(const Item * num1, const Item * num2){
    if(*num1 < *num2){
        return true;
    }else{
        return false;
    }
}

//Will determine whether the value goes to the right
bool ToRight(const Item * num1, const Item * num2){
    if(*num1 > *num2){
        return true;
    }else{
        return false;
    }
}


//Will look for a specific item/number in the tree
Family FindItem(const Item *findMe, const Tree *ptree){
    Family lookFor;
    lookFor.parent = NULL; //we set the parents to Null
    lookFor.child = ptree->root; //The child is set to the root of the tree

    if(lookFor.child == NULL){
        return lookFor; //Returning NULL, tree is empty.
    }

    while(lookFor.child != NULL){ //Until end of tree
        if(ToLeft(findMe, &(lookFor.child->number))){ //If item is found towards the left
            lookFor.parent = lookFor.child; //The parent is set to the child
            lookFor.child = lookFor.child->left; //Child is moved to the left
        }else if (ToRight(findMe, &(lookFor.child->number))){ //If item is found towards the right
            lookFor.parent = lookFor.child; //Parent is set to the child
            lookFor.child = lookFor.child->right; //Child is moved to the right
        }else{ //If child is not found to the right or left
            break; //It must be current node, so we break out the loop
        }
    }
    return lookFor;
}

//This function will add a node to the tree
void AddNode(TNode *newNode, TNode *root){
      if(ToLeft(&newNode->number, &root->number)){ //If the number belongs to the left
          if(root->left == NULL){ //If the subtree is empty
              root->left = newNode; //add note to the left
          }else{
              AddNode(newNode, root->left);//If the tree is not empty, re call function to process sub-tree
          }
      }else if (ToRight(&(newNode->number), &(root->number))){//if the number belong to the right
          if(root->right == NULL){//If subtree is empty
              root->right = newNode;//add node
          }else{
              AddNode(newNode, root->right); // else process right subtree
          }
      }else{
          perror("\nLocation error in adding node\n"); //error handling
          exit(1);
      }
}

//This function allocates space for a node
TNode * MakeNodeSpace(const Item *num){
    TNode * newNode;
    newNode = (TNode *) malloc (sizeof(TNode)); //Allocating space for node

    if(newNode != NULL){ //If memory allocation was successful.
        newNode->right = NULL; //We set the right and left pointer to nulls,
        newNode->left = NULL; // since the node will be added at the end
        newNode->number = *num; //The number is set accordingly
    }else{//Display error message
        fprintf(stderr, "\nError in node memory allocation!");
        exit(2);
    }

    return newNode; //we return the node;
}

//This function will allow one to add an item(numerical value) to the tree
bool AddItem(const Item *num, Tree * ptr){
    TNode * newNode;

    if(FindItem(num,ptr).child != NULL){ //Checking to see if item is duplicate
        fprintf(stderr, "Attempting to add duplicate item");
        return false;
    }

    newNode = MakeNodeSpace(num); //Allocating space for added node
    ptr->size++; //Incrementing tree size

    if(ptr->root ==NULL){ //if the tree is empty
        ptr->root = newNode; //root is the new node
    }else{ //If tree is not empty
        AddNode(newNode, ptr->root); //Add node normally
    }

    return true;
}

//This function will display the tree, in reverse inorder traversal
void DisplayTree(TNode *ptr, int space){

    //Base Case
    if(ptr == NULL){
        return;
    }

    space += COUNT;

    //Displaying the right child
    DisplayTree(ptr->right, space);

    //printing current node after space count
    printf("\n");
    for(int i=COUNT; i < space; ++i){
        printf(" ");
    }

    printf("%d\n", ptr->number);

    //Displaying the left child
    DisplayTree(ptr->left, space);
}
