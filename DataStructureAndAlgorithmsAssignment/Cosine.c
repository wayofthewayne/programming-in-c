#include <stdio.h>

#define ITERATIONS 5 //This will act as the upper bound of the sigma
#define PI 3.141592653589793238462643383279
double factorial(int n);
double Power(double base, int power);
double cosine(double degree, int iterations);

int main(void){
    double number;
    printf("Enter a number");
    scanf("%lf", &number);
    printf("%lf", cosine(number, ITERATIONS));
    return 0;
}

double factorial(int n){ //Will calculate the factorial
    double factorial = 1;

    for(int i=1; i<=n; ++i) {
        factorial = factorial * i;
    }
    return factorial;
}

double Power(double base, int power){ //Will calculate the power
    double result =1;

    for(int i=0; i<power; ++i){
       result *= base;
    }
    return result;
}

double cosine(double degree, int iterations){ //Will approximate the cosine function
    double ans= 0;
    double radian =  degree*(PI/180); // converts the degree to radian
    double temp = 0;

    for(int i=0; i<iterations; i++) { //This for loop will determine the number of terms of the sequence.
        temp = Power(-1, i) * (Power(radian, 2*i)/factorial(2*i)); // calculates a term from the Maclauren series
        ans += temp; // Serves as a sigma (i.e adding up the values)
    }
    return ans;
}