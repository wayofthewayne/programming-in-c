#include <stdio.h>

int SumFibonnaci(int n);
int main (void){
    int input = 15;
    printf("The sum of the fibonacci for %d is %d", input, SumFibonnaci(input));
    return 0;
}

int SumFibonnaci(int n){
    int array[n+1]; //This array will contain all the terms in the fibonacci sequence
    //We add 1, in the case of n being 0 so we do not reach an out of bounds index

    array[0] = 1; //These are the first two terms of the sequence
    array[1] = 1;

    int total = 0; //This variable will store the sum the of the sequence terms

    for(int i=2; i<n; ++i){
        array[i] = array[i-1] + array[i-2]; //Algorithm for calculating Fibonnaci sequence
        //The sum of the previous two terms
    }

    for(int i=0; i<n; ++i){ //Will add all contents of the array in a variable
        total += array[i];
    }
    return total;
}