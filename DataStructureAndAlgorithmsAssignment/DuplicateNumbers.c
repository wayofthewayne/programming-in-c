#include <stdio.h>
#include <stdbool.h>
#include <string.h>

void RepeatedIntegers(int *ar, int size);


//Please note we may use a macro (i.e ARRSIZE) and avoid the use of memcpy
//However, array size would need to be determined pre-runtime.
int main (){

    int array[] = {67,89,100,100,78,67,34,56,90};
    int arraySize = sizeof(array)/sizeof(array[0]);//retrieving number of elements in array

    for(int i=0; i<arraySize; ++i){
        printf("%d\t", array[i]);
    }
    printf("\n\n");

    RepeatedIntegers(array, arraySize);

    return 0;
}

void RepeatedIntegers(int *ar, int size){
    bool uniqueCheck[size]; // An array of bools to act as flags for repeated integers
    memset(uniqueCheck, true, size); //Initialising to true

    printf("The numbers appearing more than once in the array are:\n\n");

    for(int i=0; i<size; ++i){ //for the whole array
        if(uniqueCheck[i] == false){ //If the number is repeated, and already printed
            continue; // next iteration
        }
        for(int j=i+1; j<size; ++j){ //We start from the next number till the end of array
            if(ar[i] == ar[j]){ //When equal
                uniqueCheck[j] = false; //trigger flags
                uniqueCheck[i] = false;
            }
        }
        if(uniqueCheck[i] == false){ //if flag is triggered
            printf("\n%d", ar[i]); //number is repeated so print
        }
    }
}