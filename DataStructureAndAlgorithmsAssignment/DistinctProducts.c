#include <stdio.h>
#include <stdlib.h>

#define RANGE 1024*1024 //This will the maximum possible range for the numbers


//A pair is defined contianing 2 integers
typedef struct pair{
    int x;
    int y;
}Pair;

//We define a node as containing a pair and a pointer to the next node
typedef struct node{
    Pair twoPair;
    struct node *next;
}Node;

//Function Prototypes
Node * newNode(int i, int j);
void Display(Node **arrLL, int product);



int main (){

    int array[] = {15,64,30,2,100,1};
    int arraySize = sizeof(array)/sizeof(array[0]);

    for(int i=0; i<arraySize; ++i){
        printf("%d\t", array[i]);
    }
    printf("\n\n");

    Node * ArrLL[RANGE] = {NULL}; //Declaring an array of linked lists, and setting the to null

    for(int i=0; i<arraySize; ++i){ //Finding all pairs
        for(int j=i+1; j<arraySize; ++j){

                  int product = array[i] * array[j]; //We find their product

                  if(ArrLL[product] == NULL) { //If it is not in the table
                      ArrLL[product] = newNode(array[i], array[j]);
                  }else{ //If a collision occurs
                      //We move till we find the end of the list
                      while(ArrLL[product]->next != NULL){
                          ArrLL[product] = ArrLL[product]->next;
                      }
                      ArrLL[product]->next = newNode(array[i], array[j]);//Add node at the end of list
                  }

        }
    }

    for(int i=0; i<RANGE; ++i){ //Display for all possible products
        Display(ArrLL, i);
    }

    return 0;
}

Node * newNode(int i, int j){
    //Allocate space for node
    Node * new = (Node *) malloc (sizeof(Node));

    if(new == NULL){ //Check if memory allocation was successful
        perror("Error in adding new node");
        exit(1);
    }

    //Setting appropriate values
    new->twoPair.x = i;
    new->twoPair.y = j;
    new->next = NULL;

    return new; //returning the node
}

void Display(Node **arrLL, int product){

    //If no collision occurs
    if(arrLL[product] == NULL || arrLL[product]->next == NULL){
        return; //Nothing to print
    }else{
        printf("\n");
        Node * traverse = arrLL[product]; //Setting traverse node to current index of array

        printf("Product is %d.\n ", product);
        printf("Pairs are:\n");

        //We traverse to the end
        while(traverse != NULL){
            //Printing as we go along

            printf("(%d,%d)\t", traverse->twoPair.x,traverse->twoPair.y);
            traverse = traverse->next; //Moving to next
        }



    }
}