#include <stdio.h>
#include <stdbool.h>

#define MAX 10


//Given a selected number, the condition will check if either the number before and after are both greater
// or if they are both less. If no such conditions hold for all numbers, then the list must be sorted.
int main (void) {
    int A[MAX] = {1,5,88,34,52,6,23,78,56};
    bool sortCheck = true;

    for(int i=0; i<MAX; ++i){
        printf("%d\t", A[i]);
    }

    printf("\n\n");

    for (int i=1; i<MAX-1; i++) {
        if ((A[i-1]<A[i] && A[i]>A[i+1]) || (A[i-1]>A[i] && A[i]<A[i+1])){
            printf("%d\n", A[i]);
            sortCheck = false;
        }
    }
    if(sortCheck == true)
        printf("SORTED");
}
