#include <stdio.h>
#include <stdbool.h>

int RLargestInteger(int *ar, int size, int index);
int main(void){
    int list[] = {1,4,22,3,44,56,34,89,32,101,205};
    int listSize = sizeof(list)/sizeof(list[0]); //This will determine the size of the array

    for(int i=0; i<listSize; ++i){
        printf("%d\t", list[i]);
    }

    printf("\n\n%d", RLargestInteger(list,listSize, 0));
    return 0;
}

int RLargestInteger(int *ar, int size, int index){
    bool LargestNumber = true;

        for (int i = index; i < size; ++i) {
            if (ar[index] < ar[i]) { //Checking if the number is bigger than the rest of the numbers in the list
                //We start from the first number in the array and compare it with the rest of the numbers
                LargestNumber = false; //setting a check
            }
        }

        if(LargestNumber == true){ //if the number was the largest
            return ar[index]; // It is displayed
        }else{
            RLargestInteger(ar, size, index+1); //If it not we start with the rest of the numbers
        }
}