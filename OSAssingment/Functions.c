#include "header.h"
#include "linenoise.h"

extern char **environ; //Storage for environmental variables (& shell)

//This Function will set up the initial environment with all the variables managed by the shell
void EnvCreate(){
    char buf[256] = {'\0'};


    if(readlink("/proc/self/exe", buf, sizeof(buf)) == -1){
        perror("Error in readlink()"); //Retreiving shell value and storing it in the buffer
    }
    setenv("SHELL", buf, true); //Setting shell through readlink

    setenv("PROMPT", "eggshell v1.0>", true);//Setting the Prompt

    char *terminalName;

    if((terminalName = ttyname(STDIN_FILENO))==NULL){//retreiving terminal name
        perror("Unable to display terminal sessions");
    }
    setenv("TERMINAL", terminalName, true);//Setting terminal


    char cwd[1024];
    getcwd(cwd, sizeof(cwd));//getting the current working directory
    setenv("CWD", cwd, true);//Setting it

    setenv("EXITCODE", "0", true);//Setting exitcode

}


//This is the main parser. Here the arguments are parsed
int Parser() {
    char *line,
            *token = NULL,
            *args[MAX_ARGS];


    int tokenIndex;

    while ((line = linenoise(getenv("PROMPT"))) != NULL) { //Reading input through linenoise
        token = strtok(line, " ");

        for (tokenIndex = 0; token != NULL && tokenIndex < MAX_ARGS - 1; tokenIndex++){
            args[tokenIndex] = token; //Input is tokenised, using space as a delimeter
            token = strtok(NULL, " ");
        }

        // set last token to NULL
        args[tokenIndex] = NULL;

//The commands are passed to the function to be interpreted
        CommandRecognition(args);


// Free allocated memory
        linenoiseFree(line);
    }

}


//Prints out ouput to terminal
bool Echo(char **args, int startPoint){

    int checkForRedir = startPoint;

    while (args[checkForRedir + 1] != NULL) {  //checking for output redirection

        if (strcmp(args[checkForRedir+1], ">") == 0 && args[checkForRedir + 2] != NULL) {
            if(WriteToFile(args, startPoint, checkForRedir, "wb") == false){
                printf("Error in writing to file\n");
                return false;
            }
            return true;

        }
        if (strcmp(args[checkForRedir+1], ">>") == 0 && args[checkForRedir + 2] != NULL) {
            if(WriteToFile(args, startPoint, checkForRedir, "ab") == false){
                printf("Error in writing to file\n");
                return false;
            }
            return true;
        }

        checkForRedir++;
    }



    bool dollar = false;
    char *dollarReturn = NULL;


//We go through the arguments to echo. We start from the next argument to skip 'echo'
    while(args[startPoint +1] != NULL){

        for(int i=0; i<strlen(args[startPoint+1]); ++i){
            if(args[startPoint+1][i]=='$'){ //Once we encounter a dollar sign

                dollarReturn = DollarHandle(args,startPoint+1,i); //Function handles it
                dollar = true; //trigger bool

            }
        }


        if(dollar == true){
            printf("%s ", dollarReturn);
            startPoint++; //If the boolean was triggered, we print what the function returned

        }else{

            printf("%s ", args[startPoint+1]); //else we print normally
            startPoint++;
        }


        //Reset the boolean
        dollar = false;

    }

    printf("\n");
    return true;
}

//This Function is responsible for assinging values to Shell and env variables
bool VarChange(char *assingmentStatement){

    bool equalTrigger  = false;
    char envVar[STANDARRSIZE] = {'\0'};
    char value[STANDARRSIZE]= {'\0'};
    int counter =0;

    for(int i=0; i<strlen(assingmentStatement); ++i){


        if(assingmentStatement[i] == '='){ //When we find the equal we set of a trigger
            equalTrigger = true;
            continue; //Preventing the storing of equals
        }

        if(equalTrigger == false) { //Until equal is found we store the env var
            envVar[i] = assingmentStatement[i];
        }else{
            value[counter] = assingmentStatement[i]; // after the equals we store the respective value
            counter++; //Note the new counter needed
        }
    }

    if(value[0] == '$'){ //If dollar sign is found
        char* ptr = value;
        ptr++; //Move to next char

        ptr = getenv(ptr); //get the variable from the environment

        if(setenv(envVar, ptr, true) == -1){//Set it accordingly
            return false;
        }

    }else{

        if(setenv(envVar, value, true) == -1){ //Else retreive normally
            return false;
        }
    }
    return true;

}


//This function is responsible for dealing with the '$' character
char* DollarHandle(char **args, int startPoint, int charCount){

    char* pointerToReplacement= NULL;

    char replacement[STANDARRSIZE] = {'\0'};
    int counter = 0;
    charCount++;

    while(args[startPoint][charCount] != '\0'){
        replacement[counter] = args[startPoint][charCount];
        counter++; //We store the env var
        charCount++;
    }

    pointerToReplacement = getenv(replacement); //retreive it's value

    if(pointerToReplacement == NULL){
        printf(" \n");
    }

    return pointerToReplacement; //return it
}


//This function changes directory
bool ChangeDirectory(char *args){
    if(chdir(args)!=0) //Using the chdir() to change
        return false;

    char cwd[1024];
    getcwd(cwd, sizeof(cwd)); //The cwd is then set
    setenv("CWD", cwd, true);

    return true;
}

//Function a unsets a variable
bool Unset(char *Var){
    unsetenv(Var); //Uses unsetenv()
    return true;
}

//Function shows all env variables and sser assinged ones
void ShowEnv(bool redirectArrow, bool appendArrow, char *filename){
    int i=0;
    FILE *fp;


    if(filename != NULL){ //We check if a filename was passed

        //We then open the file in the respective mode
        if(redirectArrow == true){
            fp = fopen(filename, "wb");
        }else if(appendArrow == true){
            fp = fopen(filename, "ab");
        }

        //basic error checking
        if(fp == NULL){
            perror("Unable to open file");
            exit(CUSTOM_EXIT_CODE);
        }
    }

    while(environ[i] != NULL){ //A loop going through where the variables are stored

        if(redirectArrow == true || appendArrow == true){ //We either print on screen or in file
            fputs(environ[i], fp);
            fputs("\n", fp);
        }else{
            printf("%s\n", environ[i]); //All printed
        }
        i++;
    }
}


//Read commands from a file
void Source(char *filename){
    FILE* fp = fopen(filename, "rb"); //Opening file

    if(fp == NULL){//Checking for errors
        printf("Error in accessing file\n");
    }

    char *fileStore[STANDARRSIZE] = {'\0'};
    char lineFromFile[STANDARRSIZE] = {'\0'};
    char *token = NULL;
    int tokenIndex;

    while(fgets(lineFromFile,STANDARRSIZE, fp) != NULL){
        for(int i=0; i<strlen(lineFromFile); ++i){
            if(lineFromFile[i] == '\n' || lineFromFile[i] == '\r'){ //Replacing newline characters with null ones.
                lineFromFile[i] = '\0';
            }
        }

        token = strtok(lineFromFile, " ");

//Tokenised in the same way arguments are (space as a delimiter)
        for (tokenIndex = 0; token != NULL && tokenIndex < STANDARRSIZE - 1; tokenIndex++){
            fileStore[tokenIndex] = token;
            token = strtok(NULL, " ");
        }

        fileStore[tokenIndex] = NULL;

        CommandRecognition(fileStore);//Commands in file passed to function
    }

    fclose(fp);

}


//This function interprets the commands
bool CommandRecognition(char **args){

    bool redirectArrow = false;
    bool appendArrow = false;
    char *filename = NULL;
    char *fileData = NULL;
    char *inputRedirFile = NULL;

    for(int i=0; args[i] != NULL; ++i){
        if(args[i] != NULL && strcmp(args[i], "<") == 0  && args[i+1] != NULL){ //looking for input redirection
            inputRedirFile = args[i+1];
            fileData = ReadFileToString(inputRedirFile, "rb"); //retreiving contents of file

            int tokenIndex;
            char *token = NULL;
            char *fileContent[STANDARRSIZE];

            token = strtok(fileData, " "); //parsing file contents

            for (tokenIndex = 0; token != NULL && tokenIndex < MAX_ARGS - 1; tokenIndex++){
                fileContent[tokenIndex] = token; //Input is tokenised, using space as a delimeter
                token = strtok(NULL, " ");

            }

            // set last token to NULL
            fileContent[tokenIndex] = NULL;

            char * restArgs[STANDARRSIZE];
            restArgs[0] = NULL; //important to intiliase else garbage output might be printed
            int counter= 0;
            for(int i=0; args[i] != NULL; ++i){
                if(strcmp(args[i], ">") == 0 || strcmp(args[i], ">>") == 0){ //If there are more arguments they are stored in a temp array
                    while(args[i] != NULL){
                        restArgs[counter] = args[i];
                        i++;
                        counter++;
                    }
                    restArgs[counter] = NULL;
                    break;
                }
            }


            for(int i=0; args[i] != NULL; ++i){
                if(strcmp(args[i], "<") == 0){
                    for(int j=0; fileContent[j] != NULL; ++j){
                        args[i] = fileContent[j]; //Replacing anything after < with the file contents
                        i++;
                    }
                    for(int j=0; restArgs[j] != NULL; ++j){
                        args[i] = restArgs[j]; //putting the temp array back into the main one after the file contents
                        i++;
                    }
                    args[i] = NULL; // adding the NULL term
                    break;
                }
            }
            break;
        }
    }

    for(int i=0; args[i] != NULL; ++i){//We run a loop checking for redirections first
        //if we do find we trigger the bools and set the filenames

        if(args[i] != NULL && strcmp(args[i], ">") == 0  && args[i+1] != NULL){
            redirectArrow = true;
            filename = args[i+1];

        }else if(args[i] != NULL && strcmp(args[i], ">>") == 0  && args[i+1] != NULL){
            appendArrow = true;
            filename = args[i+1];
        }

    }

    for(int i=0; args[i]!= NULL; ++i){//Running through the arguments passed

//Check for internal commands
        if(args[i] != NULL && strcmp(args[i], "echo") == 0){
            Echo(args, i);
            break;
        }

        if (args[i] != NULL && strcmp(args[i], "exit") == 0){
            printf("Terminating...\n");
            exit(0);
        }

        if(args[i] != NULL && strcmp(args[i],"cd") == 0){
            if(args[i+1] != NULL && ( (strcmp(args[i+1], "..") == 0) || args[i+1][0] == '/')){


                if(ChangeDirectory(args[i+1]) == false){
                    perror("Unable to change directory\n");
                }

            }
            break;
        }

        if(args[i] != NULL && strcmp(args[i], "unset") == 0){
            if(args[i+1] != NULL){
                if( Unset(args[i+1]) != true)
                    perror("Error with unsetting variable\n");
            }
            break;
        }

        if(args[i] != NULL && strcmp(args[i], "showenv") == 0){
            ShowEnv(redirectArrow, appendArrow, filename);
            break;
        }

        if(args[i] != NULL && strcmp(args[i], "source") == 0){
            if(args[i+1] != NULL){
                Source(args[i+1]);
            }else{
                printf("No Filename specified!\n");
            }
            break;
        }

        bool varReturn = false;
        for(int j=0; j<strlen(args[i]); ++j){
            if(args[i][j] == '='){

                varReturn = VarChange(args[i]);

                if(varReturn == false){
                    perror("Error occured while changing variable\n");
                    return false;
                }
                break;
            }

        }

        if(varReturn == true){
            break;
        }


//Once here, the command was not an internal one, thus it is passed to be executed as an external one
        if(fork_and_exec(args,filename,redirectArrow,appendArrow) == true)
            break;
    }
}


//This function forks a process, and executes the command through the child
bool fork_and_exec(char *const *args, char* filename, bool redirectArrow, bool appendArrow) {
    // Basic error checking
    if (args == NULL || *args == NULL)
        return false;

    // Forking
    pid_t pid_ret = fork();
    //More error checking
    if (pid_ret == -1) {
        perror("fork failed\n");
        return false;
    }

    if (pid_ret == 0) {
        // This is child

        char *token = NULL;
        int tokenIndex = 0;
        char *Path = getenv("PATH");
        char completePath[STANDARRSIZE] = {'\0'};

        char *command = args[0];
        char backslash[] = {"/\0"};

        int retVal=0;

        char* EggshellSpecificVar[4];

        char cwd[STANDARRSIZE] = {"\0"}; // we will store the eggshell specific vars here
        char terminal[STANDARRSIZE] = {"\0"};
        char term[STANDARRSIZE] = {"\0"};

        strcpy(cwd, "CWD=");
        strcpy(terminal, "TERMINAL=");
        strcpy(term, "TERM=");

        strcat(cwd, getenv("CWD"));
        strcat(terminal, getenv("TERMINAL"));
        strcat(term, getenv("TERM")); //setting them in the form of VARIABLE=VALUE

        EggshellSpecificVar[0] = cwd; //Making the array of pointers to strings point to them
        EggshellSpecificVar[1] = terminal;
        EggshellSpecificVar[2] = term;
        EggshellSpecificVar[3] = NULL;


//Function will redirct output of external commands to file (if this is required)
        if(WriteExternalFile(filename, redirectArrow, appendArrow) == false)
        {
            printf("An error has occured with printing the external commands to file\n");
            return false;
        }


//We create a temp args to store the arguments, excluding > and anything after
//that is the redirection arrow and filename.
        char *tempArgs[MAX_ARGS];
        int counter =0;
        for(int i=0; args[i]!= NULL; ++i){
            if(strcmp(args[i], ">") != 0 && strcmp(args[i], ">>") != 0){
                tempArgs[counter] = args[i];
                counter++;
            }else{
                counter++;
                tempArgs[counter] = NULL;
                break;
            }
        }


        //We now get PATH tokenise it by : as a delim
        //We then attack the user entered command
        token = strtok(Path, ":");

        for (tokenIndex = 0; token != NULL; tokenIndex++){

            strcpy(completePath, token);

            strcat(completePath, backslash);
            strcat(completePath, command);

            //call exec with the full path
            retVal = execve(completePath, tempArgs, EggshellSpecificVar);

            //Note exec is run accross several paths


            token = strtok(NULL, ":");

        }

//If none of the paths match the command does not exist
        if (retVal == -1) {
            perror("Unable to execute exec\n");
            exit(CUSTOM_EXIT_CODE);
        }



    } else {
        // This is parent
        int status;

        //Parent waiting for child termination
        if (waitpid(pid_ret, &status, 0) == -1) {
            perror("waitpid failed\n");
            return false;
        }

        char ChildExitcode[15];
        snprintf(ChildExitcode, 15,"%d\n", WEXITSTATUS(status)); //Storing int to string

        // execvp failed (using custom error code 4)
        if (WIFEXITED(status) && WEXITSTATUS(status) == CUSTOM_EXIT_CODE) {
            setenv("EXITCODE", ChildExitcode, true);
            return false;
        }

        //Setting EXITCODE to child exit code
        setenv("EXITCODE", ChildExitcode, true);
    }

    return true;
}

//Function will install the handler for the respective signal
void install_handler(int signum, sig_t handler) {
    //Checking if signal function call was successful
    if (signal(signum, handler) == SIG_ERR) {
        psignal(signum, "Cannot set handler\n");
        exit(EXIT_FAILURE);
    }
}



// Print a general message when a signal is handled.
void generic_handler(int signum) {

    char msg[128];
    //Retreiving message with sys_siglist
    int n = snprintf(msg, sizeof(msg), "Signal handled: %s\n", sys_siglist[signum]);
    write(STDOUT_FILENO, msg, n < sizeof(msg) ? n : sizeof(msg));
    //Making sure message is not too big
}

//Will write to the file, in whichever mode specified.
bool WriteToFile(char **args, int startPoint, int checkForRedir, char *mode){

    FILE *fp = fopen(args[checkForRedir + 2], mode); //open the file in the specified mode

    if (fp == NULL) { //Check if opening was successful
        perror("Error in opening file\n");
        return false;
    }

    args[checkForRedir+1] = NULL; //Set the '>' to NUll

    while(args[startPoint+1] != NULL) { //Place in file

        fputs(args[startPoint+1], fp);
        fputs(" ", fp);
        startPoint++;

    }

    fclose(fp); //Close file
    return true;
}

//sets appropraite streams for output redirection for external commands
bool WriteExternalFile(char *filename, bool redirectArrow, bool appendArrow){
    if(filename != NULL && redirectArrow == true){

        int fd = open(filename, O_WRONLY| O_TRUNC | O_CREAT, S_IRUSR | S_IWUSR | S_IROTH | S_IWOTH);
        //opening file in write only, and creating file if it does not exist , and truncating it s lenght to 0 if it does.
        //allowing other and user to read and write.

        if(fd == -1){
            perror("Failed to create and open the file\n");
            exit(EXIT_FAILURE);
        }

        // make stdout go to file
        if(dup2(fd, 1) == -1){
            perror("dup2() failure\n");
            exit(EXIT_FAILURE);
        }

// make stderr go to file
        if(dup2(fd, 2) == -1){
            perror("dup2() failure\n");
            exit(EXIT_FAILURE);
        }


        close(fd); // fd no longer needed - the dup handles are sufficient

    }else if(filename != NULL && appendArrow == true){

        int fd = open(filename, O_RDWR | O_APPEND | O_CREAT, S_IRUSR | S_IWUSR | S_IROTH | S_IWOTH);
        //opening file in append only, allowing reading and writing and creating file if it does not exist.
        //Read and write allowed by owner and others

        if(fd == -1){
            perror("Failed to create and open the file\n");
            exit(EXIT_FAILURE);
        }

        // make stdout go to file
        if(dup2(fd, 1) == -1){
            perror("dup2() failure\n");
            exit(EXIT_FAILURE);
        }

// make stderr go to file
        if(dup2(fd, 2) == -1){
            perror("dup2() failure\n");
            exit(EXIT_FAILURE);
        }

        close(fd); // fd no longer needed - the dup handles are sufficient

    }

    return true;
}

long GetFileSize(FILE *f){
    int ret = fseek(f, 0L, SEEK_END); //going to end of file

    if(ret != 0){
        printf("fseek() failed\n");
        return -1;
    }

    long fileSize = ftell(f); //Getting size of file
    if(fileSize == -1){
        printf("ftell() failed\n");
        return -1;
    }

    return fileSize;
}

char *ReadFileToString(char *fileName, char *mode){

    FILE *fp = fopen(fileName, mode);

    if(fp == NULL){
        printf("fopen() failed\n");
        return NULL;
    }

    long fileSize = GetFileSize(fp);
    if(fileSize == -1){
        fclose(fp); // File would be open by now so we close
        return NULL;
    }
    char *inputString = malloc(fileSize +1); // Allocating memory +1(for null terminator)
    if(inputString == NULL){
        printf("malloc() fail!\n");
        fclose(fp);
        return NULL;
    }

    rewind(fp); //goes to the beginning of the file

    //size_t is just unsigned long using typedef to be the same on all systems in terms of bytes
    size_t numElementsRead = fread(inputString, 1, fileSize, fp); // will read the file into inputString
    assert(numElementsRead == fileSize); // to make sure all bytes in file are read

    inputString[fileSize] = '\0'; //allocating null terminator (note it is zero based index)

    fclose(fp);

    return inputString;
}
