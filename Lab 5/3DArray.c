
#define RANGE 101

void functionComputation(float ar[][RANGE][RANGE], int x);

int main (void){

    float array[RANGE][RANGE][RANGE]; // may declare it as global variable to simplify program then function would be of type void func()
    functionComputation(array, RANGE);
    return 0;

}

void functionComputation(float ar[][RANGE][RANGE], int x) {
    for (int i = 0; i < RANGE; ++i) {
        for (int j = 0; j < RANGE; ++j) {
            for (int k = 0; k < RANGE; ++k) {
                ar[i][j][k] = i + (6 * j) + (7.2 * k);
            }
        }
    }
}