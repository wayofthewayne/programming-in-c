#include <stdio.h>

int main(int argc, char *argv[]){
    int count =0;

    printf("The command line has %d arguments:\n", argc - 1);  //since argc counts the program name aswell
    for (count = 1; count < argc; count++) // starting from 1 for the same reason
        printf("%d: %s\n", count, argv[count]);

    printf("\n");
    return 0;
}