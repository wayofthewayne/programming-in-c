/* names2.c -- passes and returns structures */
#include <stdio.h>
#include <string.h>
#define NLEN 30

//struct namect was replaced BY NAME in all program
typedef struct namect{
    char fname[NLEN];
    char lname[NLEN];
    int letters;
}NAME;

NAME getinfo(void);
NAME makeinfo(NAME);
void showinfo(NAME);
char *sgets(char *str, int size);

int main(void)
{
    NAME person;
    person = getinfo();
    person = makeinfo(person);
    showinfo(person);
    return 0;
}

 NAME getinfo(void)
{
    NAME temp;
    printf("Please enter your first name.\n");
    sgets(temp.fname, NLEN);
    printf("Please enter your last name.\n");
    sgets(temp.lname, NLEN);
    return temp;
}

NAME makeinfo(NAME info)
{
    info.letters = strlen(info.fname) + strlen(info.lname);
    return info;
}

void showinfo(NAME info)
{
    printf("%s %s, your name contains %d letters.\n",
           info.fname, info.lname, info.letters);
}

char *sgets(char *str, int size){
    char *ret;
    ret = fgets(str, size, stdin);
    if (!ret)
        return ret;
    if (str[strlen(str) - 1] == '\n') {
        str[strlen(str) - 1] = '\0';
    } else {
        while (getchar() != '\n');
    }
    return ret;
}