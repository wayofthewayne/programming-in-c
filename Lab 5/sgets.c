#define STLEN 14
#include <stdio.h>
#include <string.h>

char *sgets(char *str, int size);

int main(void)
{
    char words[STLEN];

    puts("Enter a string, please.");
    sgets(words, STLEN);
    printf("Your string twice (puts(), then fputs()):\n");
    puts(words);
    fputs(words, stdout);
    puts("Enter another string, please.");
    sgets(words, STLEN);
    printf("Your string twice (puts(), then fputs()):\n");
    puts(words);
    fputs(words, stdout);
    puts("Done.");

    return 0;
}

char *sgets(char *str, int size)
{
    char *ret;
    ret = fgets(str, size, stdin);

    if(!ret)
        return ret;
    if(str[strlen(str)-1] == '\n')
    {
       // str[strlen(str)-1] = '\0'; this is commented it out to handle the \n like the previous fgets
    } else {
        while(getchar() != '\n');
    }
    return ret;
}