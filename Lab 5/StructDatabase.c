#include <stdio.h>
#include <stdbool.h>
#define LENGTH 15



//defining the struct
struct employee{
    char name[LENGTH];
    char surname[LENGTH];
    int ID;
    float monthlySalary;
};

void sort(struct employee *ar);

int v=0; //global variable


int main(void){

    int choice;

    struct employee empNo[3]; //array of structs


    do {


        printf("Would you like to add an employee?\n");
        printf("Press 1 if yes, Press 2 if no\n");
        scanf("%d", &choice);

        while(getchar() != '\n') //clearing buffer
            continue;


        if (choice == 1) {

            //accepting input
            printf("Enter employee name: \n");
            scanf("%s", empNo[v].name);

            printf("Enter employee surname: \n");
            scanf("%s", empNo[v].surname);

            printf("Enter ID: \n");
            scanf("%d", &empNo[v].ID);

            printf("Enter salary (monthly): \n");
            scanf("%f", &empNo[v].monthlySalary);

            v++; //incrementing

            while(getchar() != '\n') // clearing buffer
                continue;
        }
    }while(choice != 2);


    sort(empNo);//struct as argument
    //To be sorted by ID

    if(v>0) {//producing output
        for (int j = 0; j < v; ++j) {
            printf("%s\n", empNo[j].name);
            printf("%s\n", empNo[j].surname);
            printf("%d\n", empNo[j].ID);
            printf("%.2f\n", empNo[j].monthlySalary);
            printf("\n\n");
        }
    }else{
        printf("No books entered\n"); // this is necessary else a random number is displayed
    }


    return 0;
}

void sort(struct employee *ar){ //A bubble sort algorithm implementation
    //Accepting struct as argument
    int temp =0;
    bool swap = false;

    do {
        swap = false;
        for (int i = 0; i <v; ++i) {
            //Sorting according to ID
            if (i != (v-1) && ar[i].ID > ar[i + 1].ID) { // Swapping numbers when the number before is bigger than the one after
                //Also making sure not to point at an index outside the array bounds
                temp = ar[i].ID;
                ar[i].ID = ar[i + 1].ID;
                ar[i + 1].ID = temp;
                swap = true;
            }
        }
    }while(swap == true); // A necessary condition to make sure there is a pass with no swaps (ie all elements sorted)
}
