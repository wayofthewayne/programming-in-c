#include <stdio.h>
#include <string.h>

#define NOW 3 //Number of words
#define WL 15 //word length

int main (void) {

    char strings[NOW][WL];
    char suffix[WL];


    for (int i = 0; i < NOW; ++i) {
        printf("Enter string %d:\n", i + 1);
        gets(strings[i]);
    }

    printf("Enter a suffix: \n");
    scanf("%s", suffix);

    for(int i=0; i<NOW; ++i)
        strcat(strings[i], suffix);

    for (int i = 0; i < NOW; ++i) {
        fputs(strings[i], stdout);
        printf("\n");
    }
}