#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define DELIMITER "."


union IpAddress{
    unsigned char byte[4]; // union will reserve the most memory for the biggest type (2 of the same type will be added as a total)
    unsigned int i;
};

int main(void){

    union IpAddress ip;

    char str[64];
    strcpy(str, "192.168.23.24"); // How to intialise a string

    unsigned char byte[4]; //unsigned character has range 0 - 255, like the ip


    char *p = strtok(str, DELIMITER); // setting . as delimiter & not this returns a pointer hence why it is being stored in one

    int index =0;
    while(p != NULL){
        printf("%3s %02X\n", p, atoi(p)); //Changing the string to integer and printing it in Hex via the format specifier(X)
        //02 is for formatting 2 representing the width and 0 to fill up empty spaces with 0 for proper alignment

        ip.byte[index] = (unsigned char) atoi(p);
        index ++;

        p = strtok(NULL, DELIMITER); // it keeps on going until finding the delimiter(replacing with a null after) or null
    } // we pass null so it continues from there, if we use str it will just return 192 infinitely

    printf("\n");

    for(int i=0;i<4; ++i){ //printing it out in one line
        printf("%02X", ip.byte[i]);
    }

    printf("\n");

       //printing the same thing the as the above loop.
        printf("%02X", ip.i); // this is printed in reverse due to the way memory is stored on intel processors

    return 0;
}