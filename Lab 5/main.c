#include <stdio.h>

#define SIZE 8

void CopyArray(int const source[], int destination[], int size);

int main(void) {
    int array1[SIZE] = {4,5,6,8,9,101,56,687};
    int array2[SIZE] = {0,0,0,0,0,0,0,0};



    CopyArray(array1, array2, SIZE);

    for(int i=0; i< (sizeof(array2) / sizeof(int)); ++i) //or sizeof(array1[0])
        printf("%d\t", array2[i]);

    return 0;
}

void CopyArray(int const source[], int destination[], int size){ // note size is passed as a parameter so the size of the array can easily change

    source[size];
    destination[size];

    for(int i= 0; i < size; ++i)
        destination[i] = source[i];

}