//
// Created by Wayne Falzon on 27/10/2019.
//
/* two_func.c -- a program using two functions in one file */

#include <stdio.h>

void butler(void);      /* ISO/ANSI C function prototyping */

int main(void)
{
    printf("I will summon the butler function.\n");
    butler(); // calling function
    printf("Yes. Bring me some tea and writeable CD-ROMS.\n");
    return 0;
}



void butler(void)          /* start of function definition */
{
    char name[40];
    printf("What is your name, Sir\n");
    scanf("%s", name);
    printf("\nYou rang, %s?\n", name);
}
