//
// Created by Wayne Falzon on 27/10/2019.
//

#include <stdio.h>

int addition(int a, int b); // Prototypes
int subtraction(int a, int b);
int multiplication(int a, int b);


int main (void){
addition(4, 11);
subtraction (56, 34);
multiplication(12, 3);
return 0;
}


int addition(int a, int b){
    int add;
    add = a + b;
    printf("The addition of %d + %d = %d\n", a, b, add);
}

int subtraction(int a, int b){
    int sub;
    sub = a - b;
    printf("The subtraction of %d - %d = %d\n", a, b, sub);
}

int multiplication(int a, int b){
    int multi;
    multi = a * b;
    printf("The multiplication of %d * %d = %d\n", a, b, multi);
}