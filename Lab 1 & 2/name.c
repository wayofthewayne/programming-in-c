//
// Created by Wayne Falzon on 21/10/2019.
//
#include <stdio.h>

int main (void){
    char name[16];
    char c = 'x';

    scanf("%s", name);
    printf("%s\n", name);
    printf("%20s\n", name);
    printf("%-20s\n", name);

    printf("%c\n", c);
    printf("%d\n", c);
    printf("%o\n", c);
    printf("%x\n", c );
}
