#include <stdio.h>
#include <math.h>
#define CONSTANT 5

int main (void){

    int input;
    float output= 0, calculation;

    printf("Enter the number of iteration you would like\n");
    scanf("%d", &input);

    for(int i =1; i<=input; ++i){

        calculation = 1/(pow(i,CONSTANT)) ;
        output = output + calculation;
    }
    printf("%.5f", output);
}