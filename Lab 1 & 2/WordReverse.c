#include <stdio.h>
#include <string.h> // for strlen function

int main (void){

    char word1 [20]; // Declaring all arrays to store the words from the use input
    char word2 [20];
    char word3 [20];


 printf("Enter a word\n"); // asking and reading user input
 scanf("%s", word1);

 printf("Enter another word\n");
 scanf("%s", word2);

 printf("Enter a last word\n");
 scanf("%s", word3);


 int word1size = strlen(word1); // reading the size of the word
 int word2size = strlen(word2);
 int word3size = strlen(word3);

 int i = -1;

 printf("\n\n");

 while(i<word1size){ //used to display the words in reverse
     printf("%c", word1[word1size]);
     word1size--;
 }

 printf("\n");

 while(i<word2size){
     printf("%c", word2[word2size]);
     word2size--;
 }
printf("\n");

 while(i<word3size){
     printf("%c", word3[word3size]);
     word3size--;
 }

}

