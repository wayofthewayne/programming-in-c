#include <stdio.h>

#define WEEK 7 //Symbolic constant
#define NUM_INPUTS 10 // note the flexibility offered by this line, (easily allow us to change the repetitions)

int main(void){

    int userDays[NUM_INPUTS], days[NUM_INPUTS], weeks[NUM_INPUTS];

    for(int i=0; i<NUM_INPUTS; ++i){

        printf("Enter the number of days\n");

        scanf("%d", &userDays[i]); // use & to store in the address

        weeks[i] = userDays[i] / WEEK; // the division will the number of weeks
        days[i] = userDays[i] % WEEK; // The remainder supplied by the modulus will give the days
    }

    for(int i=0; i<NUM_INPUTS;++i) {
        printf("%d days is %d week(s) and %d day(s)\n", userDays[i], weeks[i], days[i]);
    }

    return 0;
}
