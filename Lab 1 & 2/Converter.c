#include <stdio.h>

int main (void){
    int input;
    printf("Enter an integer\n");
    scanf("%d", &input);

    printf("The integer %d corresponds to the %c character\n", input, input);
    printf("The integer %d corresponds to %o as octal and %x as Hex\n", input, input, input);
}
