#include <stdio.h> // prototype for several functions such as print and scan

int main(void) { // to declare that the main function has no parameters
    printf("Hello, World!\n");
    printf("Welcome!\n");
    return 0;
}