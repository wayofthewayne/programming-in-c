#include <stdio.h>
#define NO 3

int main (void) {

    char name[NO][15];
    char surname[NO][15] ;
    int age[NO];
    float salary[NO];
    float total, average;
    int width;

    for (int c = 0; c<NO; c++) {
        printf("Enter Name: \n");
        scanf("%10s", name[c]); //truncates afters 10 characters
        while(getchar() != '\n'); // the extra words will go to a buffer and will be put to next input. Here we clear the buffer

        printf("Enter Surname: \n");
        scanf("%10s", surname[c]);
        while(getchar() != '\n');

        printf("Enter Age: \n");
        scanf("%d", &age[c]);

        printf("Enter Salary: \n");
        scanf("%f", &salary[c]);

        total += salary[c];

    }

    average = total/NO;

    printf("Please enter the width of the salary coloumn\n");
    scanf("%d", &width);


    printf ("%20s%20s%20s%20s%*s\n", "Counter","Name","Surname","Age",width,"Salary"); // * allows user input as coloumn width

    for (int x=0;x<NO; x++)
    {

        printf("%20d%20s%20s%20d%*.2f\n", x+1,name[x], surname[x], age[x], width, salary[x]);
    }

    printf("\nThe total salary is: %.2f\n", total);
    printf("The average salary is: %.2f\n", average);


}



