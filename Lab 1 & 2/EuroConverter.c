#include <stdio.h>
#define RATE 0.90
#define ENTRIES 3

int main (void) {

    float input[ENTRIES], euro[ENTRIES];

    for (int i = 0; i < ENTRIES; ++i){
        printf("Enter the amount of dollars to be converted to euro\n");
        scanf("%f", &input[i]);

        euro[i] = input[i] * RATE;
    }

    for (int i=0; i<ENTRIES; ++i){
        printf("%.2f dollar(s) is %.2f euro(s)\n", input[i], euro[i]);
    }
}