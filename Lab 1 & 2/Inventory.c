//
// Created by Wayne Falzon on 27/10/2019.
//
#include <stdio.h>

int main(void){
int A3, A4, A5;
A3 =1000;
A4 =1000;
A5 =1000;

int orderA3, orderA4, orderA5;

printf("How many A3, A4, A5 papers would you like to order respectively\n");
scanf("%d %d %d", &orderA3, &orderA4, &orderA5);

printf("You have ordered %d A3's\n", orderA3);
printf("You have ordered %d A4's\n", orderA4);
printf("You have ordered %d A5's\n", orderA5);

A3 = A3 - orderA3;
A4 = A4 - orderA4;
A5 = A5 - orderA5;


printf("\n\nYour inventory is as follows: ");
printf("A3 %d\t", A3);
printf("A4 %d\t", A4);
printf("A5 %d\t", A5);

return 0;
}
