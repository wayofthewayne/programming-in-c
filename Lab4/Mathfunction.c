#include <stdio.h>
#include <math.h>

int MathCeil(float *num);
int MathFloor(float *num);

int main(void){
    float number;



    printf("Enter a number:");
    scanf("%f", &number);

    printf("Ceil: %d\n", (MathCeil(&number))); // passing the address of the variable in the function
    printf("Floor: %d", (MathFloor(&number)));

    return 0;
}

int MathCeil(float *num){ // passed as a pointer
    return (ceil(*num)); //  reading the value(!address) due to the *
}

int MathFloor(float *num){
    return (floor(*num));
}

