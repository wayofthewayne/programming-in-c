#include <stdio.h>
#include <string.h>
#include <ctype.h>

void inputValidity(char *str);
int lengthCheck(char *str);
char firstChar(char *str);
char mostFreq(char *str);



int main(void){

    char string[100];

    printf("Enter a string: \n");
    gets(string);

    inputValidity(string);
    printf("\nInput length is: %d\n", lengthCheck(string));
    printf("First character is: %c\n", firstChar(string));
    printf("Most frequent character is: %c", mostFreq(string));


    return 0;
}

void inputValidity(char *str){ //check for real numbers in input
    int errorCounter;
    for(int i=0 ; i< strlen(str); ++i ){
        if(isdigit(str[i]))
            errorCounter++;

    }
     printf("Integers occurrence: %d", errorCounter);
}

int lengthCheck(char *str){ // outputs the length of the string
    int count;
    return strlen(str);

}

char firstChar(char *str){ // Outputs the first character
    return str[0];
}

char mostFreq(char *str){ //Check for most frequent character
    int countMax =0;
    int prevCountMax =0;
    char frequent;

    for(int i=0; i<strlen(str); ++i){
        for(int j=0; j<i; ++j){
            if(str[i]==str[j]){ // Comparing the i(will go through all string) character with all previous characters (ie j)
                countMax++;
            }
        }

        if(countMax > prevCountMax) { // Once there is a new max we set it to the old
            prevCountMax = countMax;
            frequent = str[i]; // printing the respective character
        }
        countMax =0;
    }

    return frequent;
}


