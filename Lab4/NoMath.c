#include <stdio.h>

int MathCeil(float *num);
int MathFloor(float *num);

int main(void){
    float number;

    printf("Enter a number:", number);
    scanf("%f", &number);

    printf("Ceil: %d\n", MathCeil(&number));
    printf("Floor: %d", MathFloor(&number));


    return 0;
}

int MathCeil(float *num){
    if((*num) - ((int)(*num))  != 0){ //Checking for a remainder
            return (int)(*num)+1;
    }else{
        return (int)*num; // if the number is whole already we can just type cast it
    }
}

int MathFloor(float *num){
    return (int)(*num);
}
