//
// Created by Wayne Falzon on 11/30/2019.
//

#ifndef LAB4_HEADER_H
#define LAB4_HEADER_H



long rfact(int n); // Recursive factorial
int rFibonnaci(int n); // fibonacci recursive
int rGCD(int x, int y); // Greatest common divisor
void Dec2Hex(int no); //Decimal to hexadecimal convertor
char mostFreq(char *str);// most frequent character
int MathCeil(float *num); // Round up
int MathFloor(float *num); // Round down

#endif //LAB4_HEADER_H
