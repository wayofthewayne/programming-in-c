#include <stdio.h>

int lGCD(int x, int y);
int rGCD(int x, int y);

int main (void){

    int num1, num2;

    printf("Enter your large number: ");
    scanf("%d", &num1);

    printf("Enter the second number: ");
    scanf("%d", &num2);

    printf("Greatest common divisor (iteratively): %d\n", lGCD(num1 , num2));
    printf("Greatest common divisor (recursively): %d", rGCD(num1 , num2));
    return 0;
}

int lGCD(int x, int y){ //Iterative algorithm
    int gcd=0 ;

    for(int i=1; i <= x && i <= y; ++i){
           if(x%i==0 && y%i ==0)
               gcd = i;
    }

    return gcd;
}

int rGCD(int x, int y){
    if(y ==0) {
        return x;
    } else{
        rGCD(y, x%y);
    }

}