#include <stdio.h>
#include <string.h> // for strlen

void Dec2Hex(int no); // prototype
void RecursiveReverse(char *str);

int main(void)
{

    // i)
    int k=0;

    printf("Enter positive no:");
    scanf("%d",&k); // storing entered number
    Dec2Hex(k); // calling convertor on input

    while ( getchar() != '\n' ); // buffer clear


   //ii)

   char string[100] ;

    printf("\n\nEnter String:\n");
    gets(string); // scanf has space as delimiter

    RecursiveReverse(string);

    return 0 ;
}

void Dec2Hex(int no){
    int hex=0;

    if(!no) // till no becomes 0
        return;
    else {
        hex=no%16; // the remainder is used to print the respective hex value
        Dec2Hex(no/16);
    }

    if(hex>9) // either printing character or respective number
        printf("%c",'A'+(hex-10)); // note how the argument is an expression
    else
        printf("%d",hex);
}

void RecursiveReverse(char *str){

    if(strlen(str) == 0) // until string is 0 (the base case)
        return;

    RecursiveReverse(str + 1); // going to the next letter, we are processing the word letter by letter.
    printf("%c", *str); // we print here since we are doing this recursively
}
