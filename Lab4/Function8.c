#include "Header.h"
#include <string.h>
#include <stdio.h>

long rfact(int n)    // recursive version
{
    long ans;

    if (n > 0)
        ans= n * rfact(n-1);
    else
        ans = 1;
    return ans;
}

int rFibonnaci(int n){ //Recursive form

    if(n>2){
        return rFibonnaci(n-1) + rFibonnaci(n-2);
    }else{
        return 1;
    }
}

int rGCD(int x, int y){
    if(y ==0) {
        return x;
    } else{
        rGCD(y, x%y);
    }

}

void Dec2Hex(int no){
    int hex=0;

    if(!no) // till no becomes 0
        return;
    else {
        hex=no%16; // the remainder is used to print the respective hex value
        Dec2Hex(no/16);
    }

    if(hex>9) // either printing character or respective number
        printf("%c",'A'+(hex-10)); // note how the argument is an expression
    else
        printf("%d",hex);
}

char mostFreq(char *str){ //Check for most frequent character
    int countMax =0;
    int prevCountMax =0;
    char frequent;

    for(int i=0; i<strlen(str); ++i){
        for(int j=0; j<i; ++j){
            if(str[i]==str[j]){ // Comparing the i(will go through all string) character with all previous characters (ie j)
                countMax++;
            }
        }

        if(countMax > prevCountMax) { // Once there is a new max we set it to the old
            prevCountMax = countMax;
            frequent = str[i]; // printing the respective character
        }
        countMax =0;
    }

    return frequent;
}

int MathCeil(float *num){
    if((*num) - ((int)(*num))  != 0){ //Checking for a remainder
        return (int)(*num)+1;
    }else{
        return (int)*num; // if the number is whole already we can just type cast it
    }
}

int MathFloor(float *num){
    return (int)(*num);
}