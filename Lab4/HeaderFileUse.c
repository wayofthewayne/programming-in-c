#include <stdio.h>
#include <stdlib.h>
#include "Header.h"

int main(void){

    int choice;
    int num, num2;
    char string[20];
    float number;

    do {
        printf("\n\n\t\tMenu\n\n");
        printf("**********************************\n");
        printf("1. Factorial\n");
        printf("2. Fibonnaci\n");
        printf("3. Greatest Common Divisor\n");
        printf("4. Decimal to Hex convertor\n");
        printf("5. Most Frequent character\n");
        printf("6. Ceiling Function\n");
        printf("7. Floor Function\n");
        printf("8. Exit\n");
        printf("\n\nChoose your option: ");
        scanf("%d", &choice);

        switch (choice) {

            case 1:
                printf("Please enter a number: ");
                scanf("%d", &num);

                printf("Factorial: %ld", rfact(num));
                break;
            case 2:
                printf("Please enter a number: ");
                scanf("%d", &num);

                printf("The respective fibonnaci element is: %d", rFibonnaci(num));
                break;
            case 3:
                printf("Please enter your large number: ");
                scanf("%d", &num);

                printf("Please enter the other number: ");
                scanf("%d", &num2);

                printf("Their greatest common divisor: %d", rGCD(num, num2));
                break;
            case 4:
                printf("Enter the number you want converter to Hex: ");
                scanf("%d", &num);

                printf("In HEX: ");
                Dec2Hex(num);
                break;
            case 5:

                while ((getchar()) != '\n'); // clearing the buffer

                printf("Enter your string: \n");
                gets(string);

                printf("Your most frequent character is: %c", mostFreq(string));
                break;
            case 6:
                printf("Enter a number: ");
                scanf("%f", &number);

                printf("Ceil: %d", MathCeil(&number));
                break;
            case 7:
                printf("Enter a number: ");
                scanf("%f", &number);

                printf("Floor: %d", MathFloor(&number));
                break;
            case 8:
                exit(0);
            default:
                printf("Please enter a valid option!");
                break;
        }
    }while(choice != 8);

    return 0;
}

