#include <stdio.h>


int lFibinnoci(int n); // prototype
int rFibonnaci(int n);

int main (void){

    int choice;

    printf("Enter the number of sequences you want: ");
    scanf("%d", &choice);


    printf("Iterative Function: %d\t", lFibinnoci(choice));
    printf("Recursive Function: %d", rFibonnaci(choice));
    return 0;
}

int lFibinnoci(int n){ // iterative form

    int result = 1;
    int previous = -1;
    int sum =0;

    for(int i=0; i<=n; ++i){
        sum = result + previous ;
        previous = result;
        result = sum;
    }

    return result;
}

int rFibonnaci(int n){ //Iterative form

    if(n>2){
        return rFibonnaci(n-1) + rFibonnaci(n-2);
    }else{
        return 1;
    }
}



