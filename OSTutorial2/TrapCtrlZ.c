#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>

// sig_atomic_t ensures the use of an atomic integer
//Reading and writing this data type is guaranteed to happen in a single instruction, so there’s no way for a handler to run “in the middle” of an access.
static sig_atomic_t output_enabled = false;

static void stop_handler(int signum){
    output_enabled ^= true; //This is to toggle

    //Note without it, once we turn it to true, we won t be able to change it false
    //In this fashion we will be able to alternate as needed
}

int main() {

    //Install Handler for SIGTSTP
    if(signal(SIGTSTP, &stop_handler) == SIG_ERR){
        perror("Cannot handle SIGTSTP");
        return EXIT_FAILURE;
    }

    while(true) {
        time_t T = time(NULL); //We set a variable to store the time
        struct tm currentTime = *localtime(&T); //We retrieve the current time

        //Now we set out the layout
        char MyTime[50]; //We will store the layout here


        strftime(MyTime, sizeof(MyTime), "%d\\%a\\%b %H:%M", &currentTime); //Editing layout
        //Note we double backslash so we are able to print a bracket

        if(output_enabled == true)
            printf("%s:%d\n", MyTime, currentTime.tm_sec); //Printing with added seconds as well

        // Wait one second
        sleep(1);
    }
    return 0;
}
