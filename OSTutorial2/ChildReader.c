#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

#include "utils.h"

#define TIME_STRING_SIZE 32

// Define type sig_t for signal function prototypes
typedef void (*sig_t)(int);

// sig_atomic_t ensures the use of an atomic integer
static sig_atomic_t output_enabled = false;
static sig_atomic_t reader_mode = false;
static sig_atomic_t childReader = false;

//Signal specific handler
static void stop_handler(int signum) {
    output_enabled ^= true;
}

//To trigger this signal open another terminal and type the following command (ignore whatever is in brackets)
// pkill --signal SIGUSR1 (signal name) Tut2a3a (program name)> /dev/pts/0 (terminal name)
static void usr1_handler(int signum) {
    reader_mode ^= true;
    printf("Reader mode toggle\n");
}

static void usr2_handler(int signum){
    childReader = true;
}

//general handler
static void install_handler(int signum, sig_t handler) {
    if (signal(signum, handler) == SIG_ERR) {
        psignal(signum, "Cannot set handler");
        exit(EXIT_FAILURE);
    }
}

int main(int argc, char *argv[]) {
    // Set output_enabled if '-o' is passed as argument
    for (int i = 1; i < argc; ++i) {
        if (strcmp(argv[i], "-o") == 0) {
            output_enabled = true;
        }
    }

    // Install handler for SIGTSTP & SIGUSR1
    install_handler(SIGTSTP, stop_handler);
    install_handler(SIGUSR1, usr1_handler);
    install_handler(SIGUSR2, usr2_handler);

    // Loop until interrupt occurs
    while(true) {
        // Get the current date and time, and write them in time_str
        char time_buff[TIME_STRING_SIZE];
        char *time_str;

        if (reader_mode) { //In reader mode the time will no longer be updated

            //Finding env var type and return a pointer to string
            //This would retrieve the value of the last time DATE_TIME was updated
            time_str = getenv("DATE_TIME");

            if (time_str == NULL) {
                fprintf(stderr, "DATE_TIME environment variable does not exist\n");
                return EXIT_FAILURE;
            }

        } else {
            time_str = time_buff; //pointer pointing to array
            //Note how we always want this to point to the beginning

            if (!write_current_dt(time_buff, sizeof(time_buff))) //modify array
                return EXIT_FAILURE;

            // Save to environment variable DATE_TIME (i.e updating)
            if (setenv("DATE_TIME", time_str, true) == -1) {
                perror("Cannot set environment variable DATE_TIME");
                return EXIT_FAILURE;
            }
        }

        // Print date and time to stdout
        if (output_enabled)
            printf("%s\n", time_str); //output using pointer

        // Wait one second
        sleep(1);

        //Checking if we need to fork
        if(childReader){
            pid_t pid = fork();

            if(pid == -1){
                perror("forking failed...");
                exit(2);
            }

            if(pid == 0){ //Setting child to reader mode
                //This will, along side the current time being printed, will print the time at which the signal was called.
                //Note if it is called twice, the first time of the first signal will be printed twice.
                reader_mode = true;
            }

            //reset flag
            childReader = false;
        }

    }

    return EXIT_SUCCESS;
}