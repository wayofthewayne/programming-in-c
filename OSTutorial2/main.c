#include <stdio.h>
#include <time.h>


int main() {
    time_t T = time(NULL); //We set a variable to store the time
    struct tm currentTime = *localtime(&T); //We retrieve the current time

    //Now we set out the layout
    char MyTime[50]; //We will store the layout here


    strftime(MyTime, sizeof(MyTime), "%d\\%a\\%b %H:%M", &currentTime); //Editing layout
    //Note we double backslash so we are able to print a bracket
    printf("%s:%d\n", MyTime, currentTime.tm_sec); //Printing with added seconds as well

    return 0;
}