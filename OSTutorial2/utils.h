#pragma once

#include <stddef.h>
#include <stdbool.h>

/**
 * Writes the current date and time into string `time_str`.
 * `time_str` is the buffer of the string to write to;
 * `size_time_str` is the size of the buffer;
 * On error, this function prints to stderr and returns false;
 * returns true on success.
 **/
bool write_current_dt(char *time_str, size_t size_time_str);