#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

#include "linenoise.h"
#include "utils.h"

// Must be greater than 0
#define MAX_ARGS 16

// Must be greater than 2
#define MAX_NAME 32

// Exit code to use when execvp fails
#define CUSTOM_EXIT_CODE 4

#define TIME_STRING_SIZE 32

// Define type sig_t for signal function prototypes
typedef void (*sig_t)(int signum);

// sig_atomic_t ensures the use of an atomic integer
static sig_atomic_t alarm_triggered = false;

static void install_handler(int signum, sig_t handler) {
    if (signal(signum, handler) == SIG_ERR) {
        psignal(signum, "Cannot set handler");
        exit(EXIT_FAILURE);
    }
}

static void alrm_handler(int signum) {
    alarm_triggered = true;
}

static bool process_date_time() {
    // Populate DATE_TIME environment variable if triggered
    if (alarm_triggered) {
        // Get the current date and time, and write them in time_str
        char time_str[TIME_STRING_SIZE];
        if (!write_current_dt(time_str, sizeof(time_str)))
            return false;

        // Save to environment variable DATE_TIME
        if (setenv("DATE_TIME", time_str, true) == -1) {
            perror("Cannot set environment variable DATE_TIME");
            return false;
        }

        // Reset flag
        alarm_triggered = false;

        // Set alarm again
        alarm(1);
    }

    return true;
}

static bool fork_and_exec(char *const *args) {
    // Basic error checking
    if (args == NULL || *args == NULL)
        return false;

    // Fork and exec
    pid_t pid_result = fork();
    if (pid_result == -1) {
        perror("fork failed");
        return false;
    }

    if (pid_result == 0) {
        // This is child - call execvp
        if (execvp(args[0], args) == -1) {
            perror("execvp failed");
            exit(CUSTOM_EXIT_CODE);
        }
    } else {
        // This is parent
        int status;
        if (waitpid(pid_result, &status, 0) == -1) {
            perror("waitpid failed");
            return false;
        }

        // execvp failed (using custom error code 4)
        if (WIFEXITED(status) && WEXITSTATUS(status) == CUSTOM_EXIT_CODE) {
            return false;
        }
    }

    return true;
}

int main(void) {
    // Install signal handlers
    install_handler(SIGALRM, alrm_handler);
    //To trigger the first alarm
    alarm(1);

    char *line;
    const char *delimiter = " ";
    char *args[MAX_ARGS];
    char shell_name[MAX_NAME] = "myshell> ";

    while((line = linenoise(shell_name)) != NULL) {
        // Tokenoise
        char *token = strtok(line, delimiter);

        // Check if user wants to exit
        if (token != NULL && strcmp(token, "exit") == 0)
            return EXIT_SUCCESS;

        // Update DATE_TIME environment variable if required
        if (!process_date_time())
            return EXIT_FAILURE; //To indicate if the function was successful or not

        // Parse and copy tokens
        int i = 0;
        while(token != NULL && i < MAX_ARGS - 1) {
            args[i++] = token;
            token = strtok(NULL, delimiter);
        }

        // Argument arrays need to be null terminated
        args[i] = NULL;

        // Employ the fork+exec pattern here and update shell name
        if (i > 0 && fork_and_exec(args)) {
            int n = snprintf(shell_name, MAX_NAME, "%s> ", args[0]);
            if (n >= MAX_NAME)
                snprintf(shell_name + (MAX_NAME - 3), 3, "> ");
        }

        // Free the string allocated by linenoise
        linenoiseFree(line);
    }

    return EXIT_SUCCESS;
}