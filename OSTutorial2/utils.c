#include <stdio.h>
#include <stdbool.h>
#include <time.h>

bool write_current_dt(char * time_str, size_t size_time_str) {
    // Get time
    time_t current_time = time(NULL);
    if (current_time == (time_t)-1) {
        perror("Cannot get time");
        return false;
    }

    // Fill tm structure
    struct tm * current_time_tm = localtime(&current_time);
    if (current_time_tm == NULL) {
        fprintf(stderr, "localtime returned NULL\n");
        return false;
    }

    // Generate the date-time string we want
    if (strftime(time_str, size_time_str, "%e %b %G %H:%M:%S", current_time_tm) == 0) {
        fprintf(stderr, "strftime error: supplied buffer is too small\n");
        return false;
    }

    return true;
}