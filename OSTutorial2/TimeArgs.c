#include <stdio.h>
#include <time.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

static sig_atomic_t output_enabled = false;

static void stop_handler(int signum){
    output_enabled ^= true; //This is to toggle
    //Note without it, once we turn it to true, we won t be able to change it false
    //In this fashion we will be able to alternate as needed
}

int main (int argc, char *argv[]){

    // Set output_enabled if '-o' is passed as argument
    for (int i = 1; i < argc; ++i) {
        if (strcmp(argv[i], "-o") == 0) {
            output_enabled = true;
            break;
        }
    }

    if(signal(SIGTSTP, &stop_handler) == SIG_ERR){
        perror("Cannot handle SIGTSTP");
        return EXIT_FAILURE;
    }

    while(true) {
        time_t T = time(NULL); //We set a variable to store the time
        struct tm currentTime = *localtime(&T); //We retrieve the current time

        //Now we set out the layout
        char MyTime[50]; //We will store the layout here


        strftime(MyTime, sizeof(MyTime), "%d\\%a\\%b %H:%M", &currentTime); //Editing layout
        //Note we double backslash so we are able to print a bracket

        if(output_enabled == true)
            printf("%s:%d\n", MyTime, currentTime.tm_sec); //Printing with added seconds as well

        // Wait one second
        sleep(1);
    }



}