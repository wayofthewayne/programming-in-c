#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>

int main() {

    while(true) {
        time_t T = time(NULL); //We set a variable to store the time
        struct tm currentTime = *localtime(&T); //We retrieve the current time

        //Now we set out the layout
        char MyTime[50]; //We will store the layout here


        strftime(MyTime, sizeof(MyTime), "%d\\%a\\%b %H:%M", &currentTime); //Editing layout
        //Note we double backslash so we are able to print a bracket

        if (setenv("DATE_TIME", MyTime, true) == -1) {//Setting environment variable DATE_TIME with setenv
            perror("Cannot set environment variable DATE_TIME");
            return EXIT_FAILURE;
        }

        printf("%s:%d\n", MyTime, currentTime.tm_sec); //Printing with added seconds as well

        // Wait one second
        sleep(1);
    }
    return 0;
}
